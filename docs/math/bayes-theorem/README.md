# Bayes theorem

<p style="text-align: center">
<img src="bayes-theorem.png" width="800px" />
</p>

The "marginalization" part is also sometimes called the "evidence".

In machine learning, the marginal log-likelihood is the likelihood of the data after integrating over the latent space.

```math
p(x) = \int p(x | z) p(z) dz
```
