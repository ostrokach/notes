# Linear algebra

## Textbooks

- [The Matrix Cookbook](https://www.math.uwaterloo.ca/~hwolkowi/matrixcookbook.pdf)
- [Mathematics for Machine Learning](https://mml-book.github.io/)
- [Matrix Differentiation (and some other stuff)](https://atmos.washington.edu/~dennis/MatrixCalculus.pdf)

## Matrix properties

#### Span

<!-- prettier-ignore-start -->
If vector $`\mathbf{y}`$ is in the span of a set of $`m`$ vectors $`\mathbf{x}_1,...,\mathbf{x}_m`$, it means that it can be recreated by a linear combination of those vectors.
<!-- prettier-ignore-end -->

#### Singular matrix

- A matrix which removes some of the dimensions as part of its transformation.
- Has a null space (different inputs produce the same output).

#### Positive definite matrix

```math
x A x^T > 0
```

- This corresponds to a "bowl-shaped" matrix.
- [Real PDMs do not have to be symmetric](https://math.stackexchange.com/a/1954174/191500).
- Complex PDMs have to be [Hermitian](https://en.wikipedia.org/wiki/Hermitian_matrix) (or [self-adjoint](https://en.wikipedia.org/wiki/Self-adjoint_operator)).
- All eigenvalues are positive (as long as it is a complex Hermitian matrix or a real symmetric matrix).
- [Non-singular (invertible)](https://math.stackexchange.com/a/1059567/191500).

#### Positive semidefinite matrix

```math
x A x^T \geq 0
```

- This corresponds to a matrix with a ridge of infinite length.
- [Real PDMs do not have to be symmetric](https://math.stackexchange.com/a/1954174/191500).
- Complex PDMs have to be [Hermitian](https://en.wikipedia.org/wiki/Hermitian_matrix) (or [self-adjoint](https://en.wikipedia.org/wiki/Self-adjoint_operator)).
- All eigenvalues are non-negative (as long as it is a complex Hermitian matrix or a real symmetric matrix).

#### Real symmetric matrix

- Always has real eigenvalues.
- Possible to choose a complete set of perpendicular eigenvectors.
- [Not necessarily invertible](https://math.stackexchange.com/a/1569565/191500).

#### Hermitian matrix

- Equal to it's own [conjugate transform](#conjugate-transform).
- Complex positive (semi)definite matrices are Hermitian.
- Always has real eigenvalues (just like real symmetric matrices).

#### Gramm matrix

A matrix where every element is a dot product in some feature space.

```math
\begin{aligned}
G &= A^T A
G_{x,y} &= \phi(x) \cdot \phi(y)
\end{aligned}
```

#### Hilbert space

- Euclidean space generalized to infinite dimensions
- Can contain functions, since a function is pretty much the same as a vector of infinite length.
- Complete, meaning there are no "holes". For example, rational numbers are not complete because $`\sqrt{2}`$ is not in it. Complete means you can work with limits of sequences.
- Vectors in Hilbert space have a concept of length and inner product.
- Consider the set of all sine waves which fit an integer amount of times on a string. You can create an arbitrary function by combining these together with appropriate coefficients. So we can consider each sine wave to each be an independent dimension in a Hilbert space. The function is now a point in this abstract space.

#### Reproducing kernel Hilbert space

```math
\forall f \in \mathcal{H}, \ f(x) = \langle f(\cdot), k(x, \cdot) \rangle
```

A Reproducing Kernel Hilbert Space (RKHS) is a Hilbert space H with a reproducing kernel whose span is dense in $`\mathcal{H}`$. We could equivalently define an RKHS as a Hilbert space of functions with all evaluation
functionals bounded and linear.

- [Introduction to Machine Learning, short course on kernel methods](http://www.gatsby.ucl.ac.uk/~gretton/adaptModel/adaptModel.html)
- [Reproducing kernel Hilbert spaces in Machine Learning](http://www.gatsby.ucl.ac.uk/~gretton/coursefiles/rkhscourse.html)

## Matrix transformations

#### Conjugate transform

Take a transform and a complex-conjugate of imaginary numbers.

#### Direct sum

```math
A \oplus B
```

#### Tensor product

```math
A \otimes B
```

- Mapping which takes two inputs and produces a real number.
- Tensor is a _map_ which takes elements from the Cartesian space of vectors and produces a real number.
- Tensorts come in many forms because there are many different ways of making Cartesian products?
- <http://hitoshi.berkeley.edu/221a/tensorproduct.pdf>.

#### Eigendecomposition

- Represent a matrix in terms of its eigenvalues and eigenvectors.
- Real symmetric matrices have real eigenvalues and orthogonal eigenvectors.
- _Singular value decomposition_ extends this to non-square matrices.
- [Wikipedia](https://en.wikipedia.org/wiki/Eigendecomposition_of_a_matrix)
