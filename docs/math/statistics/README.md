# Statistics

## Resources

- [Probability Cheatsheet](probability_cheatsheet.pdf) — [[`GitHub`](https://github.com/wzchen/probability_cheatsheet)]

## Blogs

- Khan Academy — [Probability and Statistics](https://www.khanacademy.org/math/probability)
- [Setosa](http://setosa.io/#/) — Visual explanations of math, statistics and more.
- [STAC](http://persoal.citius.usc.es/manuel.mucientes/pubs/Rodriguez-Fdez15_fuzz-ieee-stac.pdf) — Statistical Tests for Algorithms Comparison.

## Parametric methods

#### One-sided t-test

- Difference between a sample mean and a (known) population mean).
- Difference between two paired samples (the population mean is 0 in this case).

#### Two-sided t-test

- Compare the difference between two sample means.
- i.e. test whether the two samples came from the same population.

## Non-parametric methods

#### Fisher's exact test

Calculates exact probability that two groups are different based on a contingency table.

#### Sign test

- Non-parametric equivalent of a [one-sided t-test](#one-sided-t-test).

#### Wilcoxon signed rank test

- Non-parametric equivalent of a [one-sided t-test](#one-sided-t-test).
- Has more power than the **sign test**.

#### Wilcoxon rank sum test

- Non-parametric equivalent of a [two-sided t-test](#two-sided-t-test).

#### Kruskal-Wallis test

- Non-parametric equivalent of **ANOVA**.

## Correlations

#### Pearson correlation (R)

- Measures linear correlation between two variables.
- Square root of the R<sup>2</sup> measure of the goodness of fit.

#### Spearman's correlation (ρ)

- Extension of Pearson's R to ordinal values.
- Captures the amount of variance that is explained by the other parameter.

#### Kendall rank correlation (τ)

- Correlation between two ordinal values.
- Based on the difference between the number of concordant and discordant pairs.
- Should be preferred over Spearman's ρ.

## Other tests

#### Dunnett test

Dunnett's test for comparing several treatments with a control.

## Bayesian methods

- [What's the difference between a confidence interval and a credible interval?](https://stats.stackexchange.com/a/2287/49160)

## Distributions

Information retrieval

- [Kolmogorov–Smirnov test](https://en.wikipedia.org/wiki/Kolmogorov%E2%80%93Smirnov_test) — Nonparametric test of the equality of continuous, one-dimensional probability distributions that can be used to compare a sample with a reference probability distribution (one-sample K–S test), or to compare two samples (two-sample K–S test).
- [Kullback–Leibler divergence](https://en.wikipedia.org/wiki/Kullback%E2%80%93Leibler_divergence) — Measure of simmilarity between two probability distributions.
- [Jensen–Shannon divergence](https://en.wikipedia.org/wiki/Jensen%E2%80%93Shannon_divergence) — Symmetric version of the Kullback–Leibler divergence.

## Information retrieval

- [RankEval](http://ufal.mff.cuni.cz/pbml/100/art-avramidis.pdf)
- [Normalized Discounted Cumulative Gain (NDCG)](https://en.wikipedia.org/wiki/Discounted_cumulative_gain) — http://stackoverflow.com/questions/23819093/distances-between-rankings
- [Mean Reciprocal Rank](https://en.wikipedia.org/wiki/Mean_reciprocal_rank)
- Expected Reciprocal Rank

## Clustering

## Enrichment

#### Gene set enrichment analysis

- [Gene set enrichment analysis: A knowledge-based approach for interpreting genome-wide expression profiles](http://www.pnas.org/content/102/43/15545.long)

## Gibbs Free Energy

- [A Gibbs free energy formula for protein folding derived from quantum statistics](http://link.springer.com/article/10.1007%2Fs11433-013-5288-x)
