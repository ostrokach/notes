# Datasets

## Compilations

- <https://github.com/caesar0301/awesome-public-datasets/>
- <https://github.com/seandavi/awesome-cancer-variant-databases/>
- <https://github.com/seandavi/awesome-single-cell/>

## Time series

- Physionet
- 2019 BEAT PD Dream Challenge (predict Parkonson's disease from sensor data).
- XTX Challenge (predict stock market tick data)

## Genome Variation

### Broad Firehose

- Download data using `{firehose client}`.

### Cosmic

- Includes data from TCGA, ICGC and manual curation.

### ExAC

- <http://exac.broadinstitute.org/>.
- Collects benign variants from many research studies.
- Paper: [Analysis of protein-coding genetic variation in 60,706 humans](http://doi.org/10.1038/nature19057)
  > "Here we describe the joint variant calling and analysis of high-quality variant calls across 60,706 human exomes, assembled by the Exome Aggregation Consortium."

### GDC

- TCGA and other data.
- Download data using `gdc-client`

### ICGC

- Download data from portal.

### T-cell receptor repertoires

- [VDJdb](https://github.com/antigenomics/vdjdb-db/releases) - VDJdb: a curated database of T-cell receptor sequences with known antigen specificity. [[paper](https://www.ncbi.nlm.nih.gov/pubmed/28977646)]

## Protein-protein interaction

- [BioGrid](https://thebiogrid.org/) — Protein-protein interaction data collected from multiple sources.
- [Mentha](https://mentha.uniroma2.it/) — Mentha is a database of protein-protein interactions. Mentha combines data from several databases in the IMEx consortium and converts gene names to UniProt identifiers.
- [STRING](https://string-db.org/) — Protein-protein interaction network, with every interaction assigned a probability score based on difference evidence channels.
- [HuRI](http://www.interactome-atlas.org/) — The Human Reference Protein Interactome Mapping Project. Genome-wide screening of pairwise interactions using yeast two-hybrid.

## Protein-small molecule interaction

- [STITCH](http://stitch.embl.de/) — Protein-small molecule interaction data integrated from multiple sources and assigned a probability score.

## Gene-disease associations

- DISEASES - Disease-gene associations mined from the literature.
- DisGeNet - Uses USMLE identifiers with mapping to DO and HPO.
- [`DECIPHER`](https://decipher.sanger.ac.uk/about#downloads/data) - Development Disorder Genotype – Phenotype Database (DDG2P) – a curated list of genes reported to be associated with developmental disorders, compiled by clinicians as part of the DDD study to facilitate clinical feedback of likely causal variants.
- PhenomeCentral
- Matchmaker Exchange
- Genomic discovery through the exchange of phenotypic and genotypic profiles - The Matchmaker Exchange: A Platform for Rare Disease Gene Discovery ( http://onlinelibrary.wiley.com/doi/10.1002/humu.22858/full)

## Ontologies

- Uberon - An integrated cross-species ontology covering anatomical structures in animals.
- HPO

## Wikipedia

Exploring human genes:

- <https://en.wikipedia.org/wiki/Lists_of_human_genes>
- <https://pageviews.toolforge.org/massviews/?project=en.wikipedia.org>
  - For "Category", can use e.g. <https://en.wikipedia.org/wiki/Category:Human_proteins> or <https://en.wikipedia.org/wiki/Category:Genes_on_human_chromosome_13>.
  - For "Wikilings", can use e.g. <https://en.wikipedia.org/wiki/List_of_human_protein-coding_genes_1>
