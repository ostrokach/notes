# Computer Science

## Algorithms

### Sorting

#### Selection sort

- Go through the list `N` times, each time swapping the `i`th element with the minimum found in the list.

#### Insertion sort

- Go through the list `N` times, each time moving `i`th element backward such that elements `0` to `i` are sorted.

#### Quick-sort

- [Khan Academy: QuickSort](https://www.khanacademy.org/computing/computer-science/algorithms#quick-sort).
