# Bioinformatics

## Courses

- [`bioinformatics-pku`](https://www.coursera.org/learn/bioinformatics-pku) - Bioinformatics: Introduction and Methods.

## Genome variant interpretation

- [Case study: Variant Discovery and Genotyping](https://github.com/hbc/edX/blob/master/edX_Notes.md)
- [In-depth NGS Data Analysis Course - Variant Calling](https://github.com/hbctraining/In-depth-NGS-Data-Analysis-Course/tree/master/sessionVI/schedule)
- [Variant Calling Pipeline: FastQ to Annotated SNPs in Hours](https://gencore.bio.nyu.edu/variant-calling-pipeline/)
- [Whole genome (10x) Analysis using bcbio-nextgen](https://bcbio-nextgen.readthedocs.io/en/latest/contents/testing.html#whole-genome-10x)

## Datasets

- http://hgdownload.cse.ucsc.edu/goldenpath/hg19/chromosomes/
- `wget -c -O NA12878_1.fastq.gz ftp://ftp.sra.ebi.ac.uk/vol1/fastq/ERR194/ERR194147/ERR194147_1.fastq.gz`
- `wget -c -O NA12878_2.fastq.gz ftp://ftp.sra.ebi.ac.uk/vol1/fastq/ERR194/ERR194147/ERR194147_2.fastq.gz`

## Tools

### Variant interpretation

- [`bcbio-nextgen`](https://github.com/bcbio/bcbio-nextgen)
- [`GEMINI`](https://gemini.readthedocs.io/) - GEMINI: a flexible framework for exploring genome variation.
- [`snpEff`](http://snpeff.sourceforge.net/) - Genomic variant annotations and functional effect prediction toolbox.

## Structure

### Pairwise alignment

- TODO: fill this section.

### Multiple sequence alignment

- [`MAFFT`](https://mafft.cbrc.jp/alignment/software/) ★ - Best tool at the moment. Has a liberal license. Supports multiprocessing. Shows state-of-the-art accuracy.
- [`MUSCLE`](http://www.drive5.com/muscle) - Maybe about as accurate as _MAFFT_, but does not support multithreading?
- [`Clustal Omega`](https://www.ebi.ac.uk/Tools/msa/clustalo/) - Reasonable performance and a good choice for making very large alignments.

**Sources:**

- [Assessing the efficiency of multiple sequence alignment programs](https://doi.org/10.1186/1748-7188-9-4)
- [Protein multiple sequence alignment benchmarking through secondary structure prediction](https://doi.org/10.1093%2Fbioinformatics%2Fbtw840)
