# Image Analysis

## Medical image analysis

[The Digital Mammography DREAM Challenge](https://www.synapse.org/#!Synapse:syn4224222)

## Dermatology

- [Dermatologist-level classification of skin cancer with deep neural networks](http://dx.doi.org/10.1038/nature21056)

## Radiology
