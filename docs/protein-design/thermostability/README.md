# Thermostability

## Background

- [Structural bioinformatics for mutation classification: scope & limitations](http://www.ebi.ac.uk/Rebholz-srv/aimm/presentations/J.Reumers_VrijeUni_Aimm_22Nov2008.pdf)

## Approaches

### Mutation effect prediction

- `EVcouplings` — [[`Source code`](https://github.com/debbiemarkslab/EVcouplings)]
- `Provean` — [`Web server 🗔`](http://provean.jcvi.org/), `GPLv3`]
- `EvolutionaryAction`
- `GERP++` - `Free software`
- `MutPred2` ★ — [_Inferring the molecular and phenotypic impact of amino acid variants with MutPred2_ (2020)](https://doi.org/10.1038/s41467-020-19669-x) [[`Web server 🗔`](http://mutpred.mutdb.org/)]<br />![citations](https://img.shields.io/badge/dynamic/json?label=citations&color=blue&query=%24%5B0%5D.count&url=https%3A%2F%2Fopencitations.net%2Findex%2Fapi%2Fv1%2Fcitation-count%2F10.1038%2Fs41467-020-19669-x)<br />"A tool that improves the prioritization of pathogenic amino acid substitutions, generates molecular mechanisms potentially causative of disease, and returns interpretable pathogenicity score distributions on individual genomes."
- `PhD SNP`
- `SNAP`
- `VEST`
- `IntOGen` — [Martínez-Jiménez et al., _A compendium of mutational cancer driver genes_ (2020)](https://doi/org/10.1038/s41568-020-0290-x) [[`Web server 🗔`](https://www.intogen.org/)]<br />![citations](https://img.shields.io/badge/dynamic/json?label=citations&color=blue&query=%24%5B0%5D.count&url=https%3A%2F%2Fopencitations.net%2Findex%2Fapi%2Fv1%2Fcitation-count%2F10.1038%2Fs41568-020-0290-x)<br />Includes predictions made using oncoDRIVE, oncoROLE, etc.

### Mutation ΔΔG prediction

- `AUTO-MUTE`
- `CUPSAT`
- `Dmutant`
- `DUET` — Metapredictor combining `SDM` and `mCSM`.
- `DynaMut`
- `ELASPIC` — [Witvliet et al., _ELASPIC web-server: proteome-wide structure-based prediction of mutation effects on protein stability and binding affinity_ (2016)](https://doi.org/10.1093/bioinformatics/btw031) [[`Web server 🗔`](http://elaspic.kimlab.org/)]<br />![citations](https://img.shields.io/badge/dynamic/json?label=citations&color=blue&query=%24%5B0%5D.count&url=https%3A%2F%2Fopencitations.net%2Findex%2Fapi%2Fv1%2Fcitation-count%2F10.1371%2Fjournal.pone.0107353) ![citations](https://img.shields.io/badge/dynamic/json?label=citations&color=blue&query=%24%5B0%5D.count&url=https%3A%2F%2Fopencitations.net%2Findex%2Fapi%2Fv1%2Fcitation-count%2F10.1093%2Fbioinformatics%2Fbtw031)
- `ELASPIC2` — [Strokach et al., _ELASPIC2 (EL2): Combining Contextualized Language Models and Graph Neural Networks to Predict Effects of Mutations_ (2021)](https://doi.org/10.1016/j.jmb.2021.166810) [[`Web server 🗔`](http://elaspic.kimlab.org/api/v2/docs)]<br />![citations](https://img.shields.io/badge/dynamic/json?label=citations&color=blue&query=%24%5B0%5D.count&url=https%3A%2F%2Fopencitations.net%2Findex%2Fapi%2Fv1%2Fcitation-count%2F10.1093%2Fj.jmb.2021.166810)
- `Eris`
- `Mechismo` — [Betts et al., _Mechismo: predicting the mechanistic impact of mutations and modifications on molecular interactions_ (2015)](https://doi.org/10.1093/nar/gku1094) [[`Web server 🗔`](http://mechismo.russelllab.org/)]<br />![citations](https://img.shields.io/badge/dynamic/json?label=citations&color=blue&query=%24%5B0%5D.count&url=https%3A%2F%2Fopencitations.net%2Findex%2Fapi%2Fv1%2Fcitation-count%2F10.1093%2Fnar%2Fgku1094)
- `FoldX`
- `INPS` — [Fariselli et al., _INPS: predicting the impact of non-synonymous variations on protein stability from sequence_ (2015)](https://doi.org/10.1093/bioinformatics/btv291)<br />![citations](https://img.shields.io/badge/dynamic/json?label=citations&color=blue&query=%24%5B0%5D.count&url=https%3A%2F%2Fopencitations.net%2Findex%2Fapi%2Fv1%2Fcitation-count%2F10.1093%2Fbioinformatics%2Fbtv291)
- `iSEE` — [Geng et al. (2018) _iSEE: Interface Structure, Evolution and Energy-based machine learning predictor of binding affinity changes upon mutations_](https://doi.org/10.1002/prot.25630)<br />![citations](https://img.shields.io/badge/dynamic/json?label=citations&color=blue&query=%24%5B0%5D.count&url=https%3A%2F%2Fopencitations.net%2Findex%2Fapi%2Fv1%2Fcitation-count%2F10.1002%2Fprot.25630)
- `MAESTRO` — [_MAESTRO - multi agent stability prediction upon point mutations_ (2015)](https://doi.org/10.1186/s12859-015-0548-6) [[`Web server 🗔`](https://pbwww.services.came.sbg.ac.at/maestro/web/)].<br />![citations](https://img.shields.io/badge/dynamic/json?label=citations&color=blue&query=%24%5B0%5D.count&url=https%3A%2F%2Fopencitations.net%2Findex%2Fapi%2Fv1%2Fcitation-count%2F10.1186%2Fs12859-015-0548-6)
- `mCSM` — [Pires et al., _mCSM: predicting the effects of mutations in proteins using graph-based signatures_ (2014)](https://doi.org/10.1093/bioinformatics/btt691) [[`Web server 🗔`](http://biosig.unimelb.edu.au/mcsm/)]<br />![citations](https://img.shields.io/badge/dynamic/json?label=citations&color=blue&query=%24%5B0%5D.count&url=https%3A%2F%2Fopencitations.net%2Findex%2Fapi%2Fv1%2Fcitation-count%2F10.1093%2Fbioinformatics%2Fbtt691)
- `PoPMuSiC`
- `PremPS` — [_PremPS: Predicting the impact of missense mutations on protein stability_ (2020)](https://doi.org/10.1371/journal.pcbi.1008543)<br />![citations](https://img.shields.io/badge/dynamic/json?label=citations&color=blue&query=%24%5B0%5D.count&url=https%3A%2F%2Fopencitations.net%2Findex%2Fapi%2Fv1%2Fcitation-count%2F10.1371%2Fjournal.pcbi.1008543)
- `SAAFEC-SEQ` — [_A Sequence-Based Method for Predicting the Effect of Single Point Mutations on Protein Thermodynamic Stability_ (2021)](https://doi.org/10.3390/ijms22020606)<br />![citations](https://img.shields.io/badge/dynamic/json?label=citations&color=blue&query=%24%5B0%5D.count&url=https%3A%2F%2Fopencitations.net%2Findex%2Fapi%2Fv1%2Fcitation-count%2F10.3390%2Fijms22020606)
- `SAAMBE` — [_Predicting Binding Free Energy Change Caused by Point Mutations with Knowledge-Modified MM/PBSA Method_ (2015)](https://doi.org/10.1371/journal.pcbi.1004276)<br />"The core of the SAAMBE method is a modified molecular mechanics Poisson-Boltzmann Surface Area (MM/PBSA) method with residue specific dielectric constant."<br />![citations](https://img.shields.io/badge/dynamic/json?label=citations&color=blue&query=%24%5B0%5D.count&url=https%3A%2F%2Fopencitations.net%2Findex%2Fapi%2Fv1%2Fcitation-count%2F10.1371%2Fjournal.pcbi.1004276)
- `SDM` — [_SDM: a server for predicting effects of mutations on protein stability_ (2017)](https://doi.org/10.1093/nar/gkx439)<br />![citations](https://img.shields.io/badge/dynamic/json?label=citations&color=blue&query=%24%5B0%5D.count&url=https%3A%2F%2Fopencitations.net%2Findex%2Fapi%2Fv1%2Fcitation-count%2F10.1093%2Fnar%2Fgkx439)
- `STRUM` — [_STRUM: structure-based prediction of protein stability changes upon single-point mutation_ (2016)](https://doi.org/10.1093/bioinformatics/btw361) [[`Web server 🗔`](https://zhanglab.ccmb.med.umich.edu/STRUM/)]<br />![citations](https://img.shields.io/badge/dynamic/json?label=citations&color=blue&query=%24%5B0%5D.count&url=https%3A%2F%2Fopencitations.net%2Findex%2Fapi%2Fv1%2Fcitation-count%2F10.1093%2Fbioinformatics%2Fbtw361)

### Thermostabilization

- `FireProt` — [_FireProt: Energy- and Evolution-Based Computational Design of Thermostable Multiple-Point Mutants_ (2015)](https://doi.org/10.1371/journal.pcbi.1004556) [[`Web server 🗔`](https://loschmidt.chemi.muni.cz/fireprot/)]<br />![citations](https://img.shields.io/badge/dynamic/json?label=citations&color=blue&query=%24%5B0%5D.count&url=https%3A%2F%2Fopencitations.net%2Findex%2Fapi%2Fv1%2Fcitation-count%2F10.1371%2Fjournal.pcbi.1004556)
  - Does not work for larger proteins (e.g. `4DKL`).
  - Protein folding only. Webserver only.
  - Predict the structural effect of multiple mutations.
  - "Stability effects of all possible single-point mutations were estimated using the <BuildModel> module of FoldX".
  - We demonstrate that thermostability of the model enzymes haloalkane dehalogenase DhaA and γ-hexachlorocyclohexane dehydrochlorinase LinA can be substantially increased.
- `PROSS` — [_Automated Structure- and Sequence-Based Design of Proteins for High Bacterial Expression and Stability_ (2016)](https://doi.org/10.1016/j.molcel.2016.06.012) [[`Web server 🗔`](http://pross.weizmann.ac.il/)]<br />![citations](https://img.shields.io/badge/dynamic/json?label=citations&color=blue&query=%24%5B0%5D.count&url=https%3A%2F%2Fopencitations.net%2Findex%2Fapi%2Fv1%2Fcitation-count%2F10.1016%2Fj.molcel.2016.06.012)<br />Protein folding only. Webserver only.
- `PROSS 2` — [_PROSS 2: a new server for the design of stable and highly expressed protein variants_ (2020)](https://doi.org/10.1093/bioinformatics/btaa1071) [[`Web server 🗔`](http://pross.weizmann.ac.il/)]<br />![citations](https://img.shields.io/badge/dynamic/json?label=citations&color=blue&query=%24%5B0%5D.count&url=https%3A%2F%2Fopencitations.net%2Findex%2Fapi%2Fv1%2Fcitation-count%2F10.1093%2Fbioinformatics%2Fbtaa1071)
- `NewProt` — [_NewProt – a protein engineering portal_ (2017)](https://doi.org/10.1093/protein/gzx024)<br />![citations](https://img.shields.io/badge/dynamic/json?label=citations&color=blue&query=%24%5B0%5D.count&url=https%3A%2F%2Fopencitations.net%2Findex%2Fapi%2Fv1%2Fcitation-count%2F10.1093%2Fprotein%2Fbtaa1071)

### Electrostatics

- `PyGBe` - [_PyGBe: Python, GPUs and Boundary elements for biomolecular electrostatics_ (2016)](https://doi.org/10.21105/joss.00043) [[GitHub](https://github.com/pygbe/pygbe)]<br />![citations](https://img.shields.io/badge/dynamic/json?label=citations&color=blue&query=%24%5B0%5D.count&url=https%3A%2F%2Fopencitations.net%2Findex%2Fapi%2Fv1%2Fcitation-count%2F10.21105%2Fjoss.00043)

## Datasets

### Protein stability

- [Cell-wide analysis of protein thermal unfolding reveals determinants of thermostability](https://doi.org/10.1126/science.aai7825).

### Mutation-induced change in protein stability

- [Protherm](http://www.abren.net/protherm/) ★ — The website seems to be down, but there are many alternative sources of this data, including [VariBench](http://structure.bmc.lu.se/VariBench/stability.php) and [Kortemme ΔΔG](https://github.com/Kortemme-Lab/ddg).
- [Rocklin et al. (2017) _Global analysis of protein folding using massively parallel design, synthesis, and testing_](https://doi.org/10.1126/science.aan0693)
- [Carlin et al. (2017) _Thermal stability and kinetic constants for 129 variants of a family 1 glycoside hydrolase reveal that enzyme activity and stability can be separately designed_](https://doi.org/10.1371/journal.pone.0176255)
- [Rockah-Shmuel et al. (2015) _Systematic Mapping of Protein Mutational Space by Prolonged Drift Reveals the Deleterious Effects of Seemingly Neutral Mutations_](https://doi.org/10.1371/journal.pcbi.1004421) — The $`W_{ref}`$ values are not provided and would have to be calculated using data in **File S2** and the process outlined in **Fig. S3**.
- [Serrano-Vega et al. (2008) _Conformational thermostabilization of the β1-adrenergic receptor in a detergent-resistant form_](https://doi.org/10.1073/pnas.0711253105) — Melting temperatures for multiple variants of the ADRB2 GPCR. Data has to be extracted from **Fig 1**.
- Taipale
- [Bahia et al. (2021) _Stability Prediction for Mutations in the Cytosolic Domains of Cystic Fibrosis Transmembrane Conductance Regulator_](https://doi.org/10.1021/acs.jcim.0c01207)

### Mutation-induced change in protein-protien interaction affinity

- [Skempi v2.0](https://life.bsc.es/pid/skempi2/) ★ — Total number of mutations: 7085.
- [IntAct Mutations](https://www.ebi.ac.uk/intact/resources/datasets#mutationDs) ★ — This dataset contains over 28,000 instances where mutations have been experimentally shown to affect a protein interaction.
- [Liu et al. (2018) _dbMPIKT: a database of kinetic and thermodynamic mutant protein interactions_](10.1186/s12859-018-2493-7) — Database of kinetic and thermodynamic mutant protein interactions. [[Data](http://deeplearner.ahu.edu.cn/web/dbMPIKT/), License: `CC BY` (according to article)]
- [Skempi](https://life.bsc.es/pid/mutation_database/)
- [AB-Bind](https://doi.org/10.1002/pro.2829) — https://github.com/sarahsirin/AB-Bind-Database
- [Mapping of the Binding Landscape for a Picomolar Protein-Protein Complex through Computation and Experiment](http://www.sciencedirect.com/science/article/pii/S0969212614000422)
- [Principles for computational design of binding antibodies](https://doi.org/10.1073/pnas.1707171114)
- [PROXiMATE: a database of mutant protein–protein complex thermodynamics and kinetics](https://doi.org/10.1093/bioinformatics/btx312) — [License: ~ `CC BY-NC`]
- [Global Edgetic Rewiring in Cancer Networks](https://doi.org/10.1016/j.cels.2015.10.006)

### Protein fitness

- [_Adaptation in protein fitness landscapes is facilitated by indirect paths._ Wu et al. (2016)](https://doi.org/10.7554/eLife.16965.001) — Fitness values (loosely correlated with stability) for almost all possible variants in four sites of the GB1 protein.

### Phenotype — benign

- [gnomAD](http://gnomad.broadinstitute.org) — [Analysis of protein-coding genetic variation in 60,706 humans](https://doi.org/10.1038/nature19057)
- [ExAC](http://exac.broadinstitute.org/)
- [dbSNP](https://www.ncbi.nlm.nih.gov/projects/SNP/)

### Phenotype — deleterious

- [OncoKB](http://oncokb.org) — Also has "likely neutral" mutations
- ClinVar - Also has "benign" / "likely benign" mutations
- UniProt [`humsavar.txt`](http://www.uniprot.org/docs/humsavar.txt)
- COSMIC
- [NIH - NCI - GDC Cancer Portal](https://portal.gdc.cancer.gov/)

### Phenotype — mutation scanning

- https://www.mavedb.org
- [Deep mutational scanning by FACS-sorting of encapsulated E. coli micro-colonies](https://dx.doi.org/10.1101/274753)
- [Exploring amino acid functions in a deep mutational landscape](https://www.biorxiv.org/content/10.1101/2020.05.26.116756v1.full) — Combined data from 28 deep mutational scanning studies, covering 6291 positions in 30 proteins. [[`Source code`](https://github.com/allydunham/aa_subtypes)]
