# Alchemistry

## Introduction

- [Binding Free Energy Calculations and Molecular Dynamics Studies on Complexes of Viral Proteases with their Ligands - Oliver Anselm Kuhn](https://ostrokach.gitlab.io/notes-private/files/Dissertation_OliverKuhn_Online.pdf)

## Software

### Simulation

#### [Sire](https://github.com/michellab/Sire)

Optional OpenMM support when running molecular dynamics simulations using `somd`.

#### [GROMACS](https://github.com/gromacs/gromacs)

See the section on free energy calculations under the [`mdp options`](http://manual.gromacs.org/online/mdp_opt.html).

- [Free Energy Calculations: Methane in Water](http://www.bevanlab.biochem.vt.edu/Pages/Personal/justin/gmx-tutorials/free_energy/01_theory.html)
- [Free Energy Calculation with GROMACS](http://www.gromacs.org/@api/deki/files/262/=gromacs-free-energy-tutorial.pdf)
- [Tutorials](https://mmb.irbbarcelona.org/biobb/)

#### AMBER

- [Toward Fast and Accurate Binding Affinity Prediction with `pmemdGTI`: An Efficient Implementation of GPU-Accelerated Thermodynamic Integration](http://pubs.acs.org/doi/pdf/10.1021/acs.jctc.7b00102)

#### OpenMM

- [OpenMM 4: A Reusable, Extensible, Hardware Independent Library for High Performance Molecular Simulation](http://pubmedcentralcanada.ca/pmcc/articles/PMC3539733/)
  - _"Derivatives of the potential are not possible in this framework, so thermodynamic integration (TI) cannot be used"_.
- [Alchemical free energy calculations in OpenMM](https://ostrokach.gitlab.io/notes-private/files/Presentation9_FreeEnergy.pdf) - Describes thermodynamic integration (TI) and Bennett acceptance ratio (BAR) and explains why multistate BAR (MBAR) is better.
- [Tinker-OpenMM: Absolute and relative alchemical free energies using AMOEBA on GPUs](http://onlinelibrary.wiley.com/doi/10.1002/jcc.24853/full) - 200x faster than a single GPU core.

### Analysis

- [FESetup](http://www.hecbiosim.ac.uk/fesetup) - Set up alchemical free energy simulations for Sire, AMBER, GROMACS, or CHARMM.
- [alchemical-analysis](https://github.com/MobleyLab/alchemical-analysis) - Tutorial from the MobleyLab on how to analyze alchemical free energy calculcations conducted in GROMACS, AMBER, or SIRE.
- [PyMBAR](https://github.com/choderalab/pymbar) - Python implementation of the multistate Bennett acceptance ratio (MBAR).
- [YANK](https://github.com/choderalab/yank) - See: [Add ability to specify protein point mutations](https://github.com/choderalab/yank/issues/69),
- [`pmx`](https://github.com/dseeliger/pmx) - Automated Protein Structure and Topology Generation for Alchemical Perturbations. [[Paper](http://doi.org/10.1002/jcc.23804), [Tutorial](https://mmb.irbbarcelona.org/biobb/)]

## Resources

- [SOMD Hydration Free Energy Tutorial](http://siremol.org/tutorials/somd/Hydration_free_energy/Production.html)
- [Accurate and Rigorous Prediction of the Changes in Protein Free Energies in a Large-Scale Mutation Scan](http://doi.org/10.1002/anie.201510054)
- [Towards computional specificity screening of DNA-binding proteins](https://doi.org/10.1093/nar/gkr531) - Validation of alchemical free energy calculation for interface mutations.
- [Calculation of Binding Free Energies](https://doi.org/10.1007/978-1-4939-1465-4_9) - Extensive protocol for alchemical free energy calculations.
