# Docking

## Protein-protein docking

### Datasets

- [`Dockground`](http://dockground.compbio.ku.edu/) — Benchmarks, decoys, templates, and other knowledge resources for docking.
- [`PDBbind`](http://www.pdbbind.org.cn/index.php) ‒ Experimentally measured binding affinities for biomolecular complexes in the PDB.

### Software

- [`DOT`](https://www.sdsc.edu/CCMS/DOT/) — FFT-based rigid body docking, with an additive correlation term to consider electrostatics. [`BSD-3-Clause`]
- [`FTDock`](http://www.sbg.bio.ic.ac.uk/docking/download.html) — FFT-based rigid body docking, with an additive correlation term to consider electrostatics. [`GPL-v2`]
- [`HADDOCK`](https://www.bonvinlab.org/software/#haddock) — Different from most other tools in that it uses CNS, optimizes the protein backbone, and allows the incorporation of experimental restraints. Flexible refinement using CNS with ambiguous interaction restraints (AIRs). [`Proprietary`]
- [`HDOCK`](http://hdock.phys.hust.edu.cn/) — FFT-based rigid body docking, with additive correlation terms and template-based scoring. One of the top servers in recent CAPRI experiments. The [`HDOCKlite`](http://huanglab.phys.hust.edu.cn/software/hdocklite/) standalone package is also available. [[📖](https://doi.org/10.1038/s41596-020-0312-x), `Proprietary`]
- [`HEX`](http://hex.loria.fr/) — Uses a spherical polar Fourier (SPF) protein docking algorithm. 1D (SPF) sampling is very marginally less successful than 3D sampling, but is much faster, especially on a GPU. [[📖](https://doi.org/10.1093/bioinformatics/btq444), [📖](https://members.loria.fr/DRitchie/papers/ritchie_limos_2013.pdf), `GPU`, `Proprietary`]
- [`LightDock`](https://github.com/lightdock/lightdock) — LightDock is a protein-protein, protein-peptide and protein-DNA docking framework based on the Glowworm Swarm Optimization (GSO) algorithm. The LightDock framework is highly versatile, with many options that can be further developed and optimized by the users: it can accept any user-defined scoring function, can use local gradient-free minimization, the simulation can be restrained from the beginning to focus on user-assigned interacting regions, it supports **residue restraints in both receptor and ligand partners**. Although not explicitly stated, it is probably slower than FFT-based methods? [`GPLv3`]
- [`MEGADOCK`](https://github.com/akiyamalab/MEGADOCK) — An ultra-high-performance protein-protein docking for heterogeneous supercomputers. [[📖](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4443796/), `GPU`, [`Apache-2 ???`](https://github.com/akiyamalab/MEGADOCK/issues/22)].
- [`PIPER`](https://www.vajdalab.org/protein-protein-docking) — FFT-based rigid body docking, with additive correlation terms. Used by the [ClusPro](https://cluspro.bu.edu/) web server, which is one of the best-performing web servers in recent CAPRI experiments. Not available for download? [[📖](https://doi.org/10.1002/prot.21117), `Proprietary`]
- [`pyDock`](https://life.bsc.es/pid/pydock/) — Used `FTDock` to generate complexes and scores those complexes using a custom function. [`Proprietary`]
- [`ZDOCK`](http://zdock.umassmed.edu/software/) ★ — FFT-based rigid body docking, with additive correlation terms to consider electrostatics and salivation. [`Proprietary`]

### References

- [Assessing Structural Predictions of Protein–Protein Recognition: The CAPRI Experiment](https://doi.org/10.1002/9781118889886.ch4) — Overview of results from recent CAPRI experiments.
- [Modeling Complexes of Transmembrane Proteins: Systematic Analysis of Protein-Protein Docking Tools](https://doi.org/10.1002/minf.201200150) — `GRAMM-X` seems to perform best, with no great difference between other tools (`ZDOCK`, `HEX`, `HADDOCK`, etc.).
- [Molecular surface recognition: determination of geometric fit between proteins and their ligands by correlation techniques](https://doi.org/10.1073/pnas.89.6.2195) — Original paper to introduce FFT-based rigid body docking.
- [Protein–Protein Docking: Overview and Performance Analysis](https://doi.org/10.1007/978-1-59745-574-9_11).
- [Protein–Protein Docking. _Lutz P. Ehrlich and Rebecca C. Wade_](https://ostrokach.gitlab.io/notes-private/files/reviews-in-computational-chemistry-17-protein-protein-docking.pdf) — Good background on traditional approaches to protein-protein docking.
- [Zhang et al. (2021) _Exploring effectiveness of ab-initio protein–protein docking methods on a novel antibacterial protein complex dataset_](https://doi.org/10.1093/bib/bbab150) — `ZDOCK` is the est, especially when taking computational resoures into consideration. `Rosetta` can be beneficial in some cases and can be used to further refine `ZDOCK` hits.

## Protein-small molecule docking

### Software

- [`AutoDock Vina`](http://vina.scripps.edu/) — MC based docking software. Free for academic usage. Flexible ligand. Flexible protein side chains. Maintained by the Molecular Graphics Laboratory, The Scripps Research Institute, la Jolla. [`Apache-2`]
- [`GNINA`](https://github.com/gnina/gnina) — gnina (pronounced NEE-na) is a molecular docking program with integrated support for scoring and optimizing ligands using convolutional neural networks. It is a fork of _smina_, which is a fork of _AutoDock Vina_. [[`YouTube 🎥`](https://youtu.be/MG3Srzi5kZ0), `Apache-2`]
- [`iDock`](https://github.com/HongjianLi/idock) — idock is a standalone tool for structure-based virtual screening powered by fast and flexible ligand docking. Fork of `AutoDock Vina`. [`Apache-2`]
- [`smina`](https://sourceforge.net/projects/smina/) — A fork of AutoDock Vina that is customized to better support scoring function development and high-performance energy minimization. smina is maintained by David Koes at the University of Pittsburgh and is not directly affiliated with the AutoDock project. [`Apache-2`]

### References

- [Software for molecular docking: a review](https://dx.doi.org/10.1007%2Fs12551-016-0247-1https://dx.doi.org/10.1007%2Fs12551-016-0247-1).
