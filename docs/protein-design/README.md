# Getting started

## Loading protein structures

- [`ESBTL`](http://esbtl.sourceforge.net/) → Efficient PDB parser and data structure for the structural and geometric analysis of biological macromolecules. Good integration with `CGAL`. (doi: [`10.1093/bioinformatics/btq083`](https://doi.org/10.1093/bioinformatics/btq083)) [`C++`]
- [`gemmi`](https://github.com/project-gemmi/gemmi) ★ → Library for reading and writing PDB and mmCIF files. Also has support for generating biological assemblies, applying basic transformations, performing structural alignment, calculating dihedral angles, etc.. [`C++`, `Python`]

### Homology modeling

- [`MODELLER`](https://salilab.org/modeller/) → Most-widely used homology modeling software. [`Python`, `Proprietary`]
- [`ProMod3`](https://git.scicore.unibas.ch/schwede/ProMod3) → A free and open source homollogy modeling toolbox powering the SWISS-MODEL server. Uses `OPENMM` for some refinement. [`LGPL`]<br>[_ProMod3—A versatile homology modelling toolbox_ (2021)](https://doi.org/10.1371/journal.pcbi.1008667)
- AlphaFold2...

### Structural alignment

- [Cealign](https://pymolwiki.org/index.php/Cealign_plugin) - PyMol plugin.

### Structural visualization

- [`3Dmol.js`](https://github.com/3dmol) → WebGL accelerated JavaScript molecular graphics library. The Jupyter widget works in Google Colab.
- [`LiteMol`](https://github.com/dsehnal/LiteMol) → A library/plugin for handling 3D structural molecular data (not only) in the browser.
- [`Mol*`](https://github.com/molstar/molstar) ★ → A comprehensive macromolecular library. Developed to combine and build on the strengths of LiteMol and NGL.
- [`NGL`](https://github.com/nglviewer/ngl) → WebGL protein viewer. There is also a Jupyterlab [widget](https://github.com/nglviewer/nglview) with bidirectional information flow.
