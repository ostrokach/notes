# Optimization and design

## Overview

The two most widely-used software packages for protein design are Rosetta and (to a much lesser extent) Osprey.

## Rosetta

### Background

- [Core Rosetta Concepts](https://www.rosettacommons.org/demos/latest/tutorials/Core_Concepts/Core_Concepts)

### Weights

- `talaris2013`
- `talaris2014`
- `beta_nov15`

### Protocols

**_relax_**

**_score_**

**_fixbb basic_**

```bash
fixbb.static.linuxgccrelease \
    -in:file:s '{structure_file}' \
    -database '{rosetta_db}' \
    -dun10 \
    -scorefile 'default_{structure_file}' \
    -out:suffix '_def' \
    -beta_nov16_cart
```

**_fixbb extended_**

```bash
fixbb.static.linuxgccrelease \
    -in:file:s '{structure_file}' \
    -database '{rosetta_db}' \
    -resfile '{res_file}' \
    -dun10 \
    -ex1 \
    -ex2 \
    -extrachi_cutoff 0  \
    -minimize_sidechains True \
    -scorefile 'fixbb_{structure_file}' \
    -beta_nov16_cart \
    -relax:fast
```

**_ddg_monomer_**

<https://www.rosettacommons.org/docs/latest/application_documentation/analysis/ddg-monomer>

```bash
ddg_monomer.static.linuxgccrelease \
    -in:file:s '{structure_file}' \
    -in::file::fullatom \
    -database '{rosetta_db}' \
    -ignore_unrecognized_res true \
    -ignore_zero_occupancy false \
    -fa_max_dis 9.0 \
    -ddg::weight_file soft_rep_design \
    -ddg::mut_file '{mutation_file}' \
    -ddg::iterations 50 \
    -ddg::local_opt_only true \
    -ddg::dump_pdbs true \
    -ddg::suppress_checkpointing true \
    -ddg::mean true \
    -ddg::min true \
    -ddg::output_silent true
```

## Osprey

- [LUTE (Local Unpruned Tuple Expansion): Accurate continuously flexible protein design with general energy functions and rigid-rotamer-like efficiency](http://www.cs.duke.edu/donaldlab/Supplementary/recomb16/lute/LUTE.pdf)
