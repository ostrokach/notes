# Conformational sampling

## Backbone ensembles

- [ENCoM](https://github.com/NRGlab/ENCoM)
  - Paper: [A Coarse-Grained Elastic Network Atom Contact Model and Its Use in the Simulation of Protein Dynamics and the Prediction of the Effect of Mutations](http://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1003569).
  - Protocol: [Applications of Normal Mode Analysis Methods in Computational Protein Design](https://link.springer.com/protocol/10.1007/978-1-4939-6637-0_9#Sec1).
  - License: `GPL` (according to protocol paper)
- [CONCOORD](http://www3.mpibpc.mpg.de/groups/de_groot/concoord/) - CONCOORD is a method to generate protein conformations around a known structure based on geometric restrictions.
- [tCONCOORD](http://wwwuser.gwdg.de/~dseelig/tconcoord.html) - tCONCOORD predicts protein conformational flexibility based on geometrical considerations.

**See also:**

- [Protein Dynamics : From Structure to Function](https://doi.org/10.1007/978-94-024-1069-3_12) - Description of CONCOORD and tCONCOORD.

## Machine learning

### Boltzmann generators

