# RNA

## Inverse folding problem

The goal of the _inverse RNA folding problem_ is to generate RNA sequences which fold into a pre-determined shape.

**Note:** The inverse RNA folding problem may be more of a sampling problem than a constraint satisfaction problem, since the number of constraints per ribonucleotide is much smaller but the number of off-target foldings is much higher.

### Datasets

- [RNA STRAND v2.0](http://www.rnasoft.ca/strand/) - Contains ~5000 RNA sequences and base-pairings. Has not been updated in ~10 years.
- [bpRNA-1m](http://bprna.cgrb.oregonstate.edu) - Contains ~100,000 RNA sequences and base-pairings. Published in 2018.
- [Eterna100](https://github.com/jadeshi/SentRNA/tree/master/data/test) - A representative dataset of RNA structures solved by Eterna players.
- [RNAiFold 2.0](http://bioinformatics.bc.edu/clotelab/RNAiFold2.0/index.php?tab=downloads) - 63 experimentally-synthesized targets.
- CASP RNA Challenge — CASP is planning to include RNA puzzles in the future.

### Software

- [Vienna](https://www.tbi.univie.ac.at/RNA/)
- [RNAStructure](https://rna.urmc.rochester.edu/RNAstructure.html) - Contains useful utilities, including `dot2ct` and `ct2dot`, which allow the conversion between the [CT file format][rna file formats] and the [Dot Bracket file format][rna file formats].
- [EternaBrain](https://github.com/Eternagame/EternaBrain) - Source code for `EternaBrain-SAP`.
- [CDPfold](https://github.com/zhangch994/CDPfold) - Solve the inverse RNA folding problem using reinforcement learning.
- [jadeshi/SentRNA](https://github.com/jadeshi/SentRNA) - Solve the inverse RNA folding problem using reinforcement learning.

[rna file formats]: https://rna.urmc.rochester.edu/Text/File_Formats.html
