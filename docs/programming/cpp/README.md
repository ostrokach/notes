# C++

## Guidelines

### Overview

- [Avoid unsigned integers](http://wesmckinney.com/blog/avoid-unsigned-integers/)
- [C++ Core Guidelines](https://github.com/isocpp/CppCoreGuidelines/blob/master/CppCoreGuidelines.md)
- [C++11/14/17](https://github.com/AnthonyCalandra/modern-cpp-features)
- [Google Style guide](https://google.github.io/styleguide/cppguide.html)

### Pass by reference vs. pass by pointer

It seems that the consensus is to "use references when you can and pointers when you have to". More specifically:

- Use "pass by reference" unless you have a good reason not to.
- Use "pass by pointer" if `NULL` is a possible value.

**Sources:**

- [StackOverflow: When to pass by reference and when to pass by pointer in C++?](https://stackoverflow.com/questions/3613065/when-to-pass-by-reference-and-when-to-pass-by-pointer-in-c)

Passing by reference also works well with `std::unique_ptr` and `std::shared_ptr`. In this case, the function can be implemented irrespective of how the user manages the object lifetime.

> As usual, use a `*` if you need to express null (no widget), otherwise prefer to use a `&`; and if the object is input-only, write `const widget*` or `const widget&`.

**Sources:**

- [GotW #91 Solution: Smart Pointer Parameters](https://herbsutter.com/2013/06/05/gotw-91-solution-smart-pointer-parameters/)
- [StackOverflow: How to pass std::unique_ptr around?](https://stackoverflow.com/questions/11277249/how-to-pass-stdunique-ptr-around)
- [StackOverflow: Should we pass a shared_ptr by reference or by value?](https://stackoverflow.com/questions/3310737/should-we-pass-a-shared-ptr-by-reference-or-by-value)

### Application-binary interface (ABI)

The C++ ABI is reasonably stable, but when a non-backwards compatible change is introduced, all libraries need to be recompiled in order to be able to work with the new ABI.

## Awesome

### General

- [protobuf](https://github.com/protocolbuffers/protobuf) — C++ does not really have "data classes" in the same way that you have data classes in Python or Kotlin, but you can use protobufs instead.

### Linear algebra

- [`ATLAS`](http://math-atlas.sourceforge.net/) - Automatically Tuned Linear Algebra Software. Provides full BLAS and certain LAPACK routines, which are being tuned to the computer platform at the compilation time. Slower than vendor-provided BLAS such as MKL.
- [`BLAS`](http://www.netlib.org/blas/) - Basic Linear Algebra Subprograms.
- [`LAPACK`](http://www.netlib.org/lapack/) - LAPACK (Linear Algebra PACKage) provides routines for solving systems of simultaneous linear equations, least-squares solutions of linear systems of equations, eigenvalue problems, and singular value problems. It runs on single processor only.
- [`OpenBLAS`](http://www.openblas.net/) - ([Source Code](https://github.com/xianyi/OpenBLAS))
- [`MKL`](https://software.intel.com/en-us/intel-mkl) - Intel BLAS/LAPACK implementation.

### Numerical Libraries

- [`ArrayFire`](https://arrayfire.com) - A general purpose GPU library. - ([Source Code](https://github.com/arrayfire/arrayfire)) `BSD`
- [`Blaze`](https://bitbucket.org/blaze-lib/blaze) - Blaze is an open-source, high-performance C++ math library for dense and sparse arithmetic. With its state-of-the-art Smart Expression Template implementation Blaze combines the elegance and ease of use of a domain-specific language with HPC-grade performance, making it one of the most intuitive and fastest C++ math libraries available.
- [`Deal.II`](http://www.dealii.org/) - Solve partial differential equations using the finite element method. ([Source Code](https://github.com/dealii/dealii)) `C++` `GPL`
- [`Eigen`](http://eigen.tuxfamily.org/) - Used by TensorFlow :). ([Source Code](https://github.com/RLovelett/eigen))
- [`FLENS`](http://apfel.mathematik.uni-ulm.de/~lehn/FLENS/) - FLENS extends C++ for matrix/vector types that are ideally suited for numerical linear algebra. Matrix/vector types for dense linear algebra. Generic implementation of BLAS / LAPACK.
- [`GSL`](https://www.gnu.org/software/gsl/) - The GNU Scientific Library (GSL) is a numerical library for C and C++ programmers. The library provides a wide range of mathematical routines such as random number generators, special functions and least-squares fitting. While GSL is not parallel, it is reasonably thread safe and its routines should be callable from parallel code sections. One can also link a parallel BLAS library such as MKL or ACML and utilize the shared memory parallelism they provide. Has Python bindings: [pygsl](https://sourceforge.net/projects/pygsl/).
- [`PETSc`](https://www.mcs.anl.gov/petsc/) - Solvers for partial and ordinary differential equations. Parallel vectors / matrices. - ([Source Code](https://bitbucket.org/petsc/petsc))
- [`Root`](https://root.cern.ch/) - A modular scientific software framework. It provides all the functionalities needed to deal with big data processing, statistical analysis, visualisation and storage. It is mainly written in C++ but integrated with other languages such as Python and R.
- [`Trilinos`](https://trilinos.org) - Shared memory data structures. Includes packages of other code. ([Source Code](https://github.com/trilinos/Trilinos))

### Image processing

- [`CImg`](http://cimg.eu/) - The CImg Library is a small, open-source, and modern C++ toolkit for image processing. - ([Source Code](https://github.com/dtschump/CImg)).

### Utilities

- `nm my_binary | c++filt | grep VAR` to get names.
- `readelf`

## Build system

### Make

- Use `.PHONY` targets when the script does not produce a file with the particular name.
- Use '\' run multiple bash commands using the same bash interpreter.
- https://www.gnu.org/software/make/manual/html_node/index.html

### CMake

- Writing `find_package()` in CMake: https://cmake.org/Wiki/CMake:How_To_Find_Libraries
- `CMAKE_INSTALL_PREFIX`
- [`CMAKE_INCLUDE_PATH`](https://cmake.org/cmake/help/v3.0/variable/CMAKE_INCLUDE_PATH.html)
- `FIND_FILE()` / `FIND_PATH()`
- [`include_directories`](https://cmake.org/cmake/help/v3.0/command/include_directories.html)
- `include_directories( ${CMAKE_SOURCE_DIR}/include )` from any subfolder
- `add_library`
- `add_executable`
- [`target_include_directories`](https://cmake.org/cmake/help/v3.0/command/target_include_directories.html)
- [`CMAKE_INSTALL_PREFIX`](https://cmake.org/cmake/help/v3.0/variable/CMAKE_INSTALL_PREFIX.html)
- [`install`](https://cmake.org/cmake/help/v3.0/command/install.html)

### Meson

### Ninja

Good alternative to Make for building projects. CMake can easily be configured to generate Ninja build files instead of Make.

## Testing

- [Boost.Test](https://github.com/boostorg/test) - `Boost`
- [Catch](https://github.com/philsquared/Catch) - A modern, C++-native, header-only, framework for unit-tests, TDD and BDD. C++ Automated Test Cases in Headers. `Boost`
- [Google Test](https://github.com/google/googletest) ★★★ - Most used by companies? `2-clause BSD`
