# Latex

## Installing packages from CTAN

1. Download `{package_name}.tds.zip` from the [CTAN website](https://www.ctan.org/).
2. Unzip the archive inside the `/usr/local/share/texmf` directory (tested on Ubuntu 16.04).
3. Run `sudo texhash`.

### Allow tables to stretch past `\textwidth`

```latex
\addtolength{\leftskip}{-0.03\textwidth}
\addtolength{\rightskip}{-0.03\textwidth}
\begin{tabular}{m{0.5\textwidth} | m{0.5\textwidth}}
...
\end{tabular}
```

## Some useful packages

### fontspec

Allows us to use custom true type and otf fonts in our documents. Only available when using `xelatex`.

```latex
\usepackage{fontspec}

\newfontfamily\FontAwesomeProRegular{FontAwesome5Pro-Regular-400.otf}[Path = fonts/]
\newfontfamily\FontAwesomeProLight{FontAwesome5Pro-Light-300.otf}[Path = fonts/]
\newcommand{\falServer}{{\FontAwesomeProLight }}
\newcommand{\falTextAlt}{{\FontAwesomeProLight }}
\newcommand{\falDatabase}{{\FontAwesomeProLight }}
\newcommand{\falCodeBranch}{{\FontAwesomeProLight }}
\newcommand{\falFileChartLine}{{\FontAwesomeProLight }}
```

### `markdown`

https://www.ctan.org/pkg/markdown

```latex
\documentclass{article}
\usepackage{markdown}
\markdownSetup{
  renderers = {
    link     = {#1},        % Render a link as the link label.
    emphasis = {\emph{#1}}, % Render emphasis using `\emph`.
  }
}
\begin{document}
\begin{markdown}
  _Hello,_ [Stack Exchange](http://tex.stackexchange.com)!
\end{markdown}
\end{document}
```

**See also:** https://tex.stackexchange.com/a/334851/66387

### `lstlisting`

Insert code

```latex
\begin{lstlisting}[caption={Demo}]
import os
print(os.listdir())
\end{lstlisting}
```

## Installation

- [StackOverflow: How to install “vanilla” TeXLive on Debian or Ubuntu?](https://tex.stackexchange.com/questions/1092/how-to-install-vanilla-texlive-on-debian-or-ubuntu)

```bash
sudo apt-get install texlive-latex-base texlive-latex-extra texlive-latex-recommended texlive-extra-utils
sudo apt install lmodern
# Microsoft fonts
sudo apt install msttcorefonts ttf-mscorefonts-installer
#
sudo apt-get install xzdec
sudo tlmgr init-usertree
sudo tlmgr update --all
```

## Bibliography

![bibliography_stack](bibliography_stack.png)

- [TeX StackExchange: bibtex vs. biber and biblatex vs. natbib ](https://tex.stackexchange.com/questions/25701/bibtex-vs-biber-and-biblatex-vs-natbib)

### LaTeX packages

- **biblatex** - Defines macros for citing inside _.tex_ documents.
  BibLaTeX is the succesor format of BibTeX. Can work with _.bib_ files processed with **biber** or **BibTeX**.
- **natbib** - _.bib_ files need to be processed with **BibTeX**.

### Processing program

Processes _.bib_ files into something a _.tex_ file can understand.

- **biber** - Supports unicode. Can use more features for the _.bib_ input format.
- **BibTeX**
-

### Database file

## Packages

- [beamer] - LaTeX class to create powerful, flexible and nice-looking presentations and slides. ([ShareLatex](https://www.sharelatex.com/learn/Beamer))
- [beamerposter](https://www.ctan.org/pkg/beamerposter) - ([Source Code](https://github.com/deselaers/latex-beamerposter))
  - [Writing posters with beamerposter package in LATEX](http://tug.org/pracjourn/2012-1/shang/shang.pdf)
- [TikZ]() - ([Wiki](https://en.wikibooks.org/wiki/LaTeX/PGF/TikZ))

## Minipage

```latex
% \begin{minipage}[vertical centering][height]{width}
\begin{minipage}[c][7in]{1.0\textwidth}
  \centering
  \includegraphics[width=1.0\textwidth,height=4in,keepaspectratio]{static/elaspic_training_set/validation/test_performance_core.pdf}
  \caption{...}
\end{minipage}
```

## Templates

- [Conference Posters](https://www.latextemplates.com/cat/conference-posters)
  - [Jacobs Landscape Poster](https://www.latextemplates.com/template/jacobs-landscape-poster)
  - [Jacobs Landscape Poster Mod](http://www.njohnston.ca/2009/08/latex-poster-template/)
  - [baposter Landscape Poster](https://www.latextemplates.com/template/baposter-landscape-poster)

## Posters

- [How to create posters using LaTeX](https://tex.stackexchange.com/questions/341/how-to-create-posters-using-latex) - Many options: [beamerposter](http://www.ctan.org/pkg/beamerposter), [tikzposter](http://www.ctan.org/pkg/tikzposter), [baposter](http://www.brian-amberg.de/uni/poster/), [sciposter](http://www.ctan.org/pkg/sciposter), [a0poster](http://www.ctan.org/pkg/a0poster).
