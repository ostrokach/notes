# Web frameworks

## Static site generators

Comparison of static site generators: <https://www.staticgen.com/>.

### Go

#### Hugo

- [Themes discussion](https://discuss.gohugo.io/t/hugo-as-a-documentation-tool/112/33)
- [Material theme](https://github.com/digitalcraftsman/hugo-material-docs)
- [Documentation themes](http://themes.gohugo.io/tags/documentation)

### JavaScript

#### [GitBook]

- Need to include every markdown file that you want rendered inside the `SUMMARY.md` file (ok, just need to write a script to do this...).

#### [Gatsby]

### Python

#### MkDocs

- [![](https://img.shields.io/github/stars/mkdocs/mkdocs.svg?style=social)](https://github.com/mkdocs/mkdocs)
- Does not support relative links to images using \<img\> tags (problem with '..' offset, see mkdocs/mkdocs#991).
- Themes:
  - [Material theme](https://github.com/squidfunk/mkdocs-material)

#### Sphinx

- [![](https://img.shields.io/github/stars/sphinx-doc/sphinx.svg?style=social)](https://github.com/sphinx-doc/sphinx)
- Does not support relative links to images using \<img\> tags (images get moved to a separate images).
- Themes:
  - [sphinx_graphene_theme](https://github.com/graphql-python/graphene-python.org/tree/docs)

`sphinx.ext.autosummary` example:

`submodule/__init__.py`:

```python
"""
Submodule
=========

hello world
"""

class SomeOther(object):
    """Some other class"""

    #: First class attribute
    x: str

    #: Second class attribute
    y: str

#: This is attribute of submodule, of type boolean
ATTRIBUTE_OF_SUBMODULE = True

OTHER_ATTRIBUTE_OF_SUBMODULE = SomeOther()
"""This is *other* attribute of type :class:`SomeClass`"""
```

`docs/conf.py`:

```rst
.. automodule:: submodule

.. autosummary::

    ATTRIBUTE_OF_SUBMODULE
    OTHER_ATTRIBUTE_OF_SUBMODULE
    SomeOther

.. autoclass:: SomeOther
    :members:
```

## Math

- https://github.com/cben/mathdown/wiki/math-in-markdown

recommonmark has the same math formula syntax as GitLab flavoured markdown.

## Sphinx

### Themes

- [Endorsed sphinx themes](http://www.writethedocs.org/guide/tools/sphinx-themes/)
- [sphinx_rtd_theme](https://github.com/rtfd/sphinx_rtd_theme)
- [guzzle_sphinx_theme](https://github.com/guzzle/guzzle_sphinx_theme)

### Extensions

- nbsphinx

## Jupyter

### Templates

- templates which come with [jupyter_contrib_nbextensions](https://github.com/ipython-contrib/jupyter_contrib_nbextensions/tree/master/src/jupyter_contrib_nbextensions/templates)

## Presentation frameworks

- [decktape](https://github.com/astefanutti/decktape) - Save common javascript presentation types to PDF using qt webkit. The README file contains a list of good javascript slide frameworks that are supported.
- [revealjs](https://github.com/hakimel/reveal.js/) - The HTML Presentation Framework.
- [spectacle](https://github.com/FormidableLabs/spectacle) - Another presentation framework, based on react. More actively developed than `revealjs`.

A good workflow for slides is `old way` → `problem` → `solution`.

## Flowchart diagrams

- [sketchviz](https://sketchviz.com/) — Hand-drawn theme for graph-viz.
- [mermaid](https://github.com/knsv/mermaid) — Generation of diagram and flowchart from text in a similar manner as markdown<br>![](https://img.shields.io/github/stars/knsv/mermaid.svg?style=social)

 http://knsv.github.io/mermaid/

#### [flowchart.js ![](https://img.shields.io/github/stars/adrai/flowchart.js.svg?style=social)](https://github.com/adrai/flowchart.js)

Draws simple SVG flow chart diagrams from textual representation of the diagram http://flowchart.js.org/

#### [js-sequence-diagrams ![](https://img.shields.io/github/stars/bramp/js-sequence-diagrams.svg?style=social)](https://github.com/bramp/js-sequence-diagrams)

Draws simple SVG sequence diagrams from textual representation of the diagram https://bramp.github.io/js-sequence-diagrams/

## Wiki engines

### Python

#### [django-wiki ![](https://img.shields.io/github/stars/django-wiki/django-wiki.svg?style=social)](https://github.com/django-wiki/django-wiki)

A wiki system with complex functionality for simple integration and a superb interface. Store your knowledge with style
Use django models. https://demo.django.wiki

#### [realms-wiki ![](https://img.shields.io/github/stars/scragg0x/realms-wiki.svg?style=social)](https://github.com/scragg0x/realms-wiki)

Git based wiki inspired by Gollum http://realms.io

#### [waliki ![](https://img.shields.io/github/stars/mgaitan/waliki.svg?style=social)](https://github.com/mgaitan/waliki)

A wiki engine powered by Django and Git http://waliki.pythonanywhere.com

#### [MoinMoin](https://bitbucket.org/thomaswaldmann/moin-2.0)

Still alpha but [many great features](https://moinmo.in/MoinMoin2.0) planned (including Markdown and ReST support)

#### [XWIKI ![](https://img.shields.io/github/stars/xwiki/xwiki-platform.svg?style=social)](https://github.com/xwiki/xwiki-platform)

The most popular Java framework. Used by PhenoTips. Taylored towards the enterprise folk.
