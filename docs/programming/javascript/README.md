# Javascript

https://stateofjs.com

## Web frameworks

- [`htmx`](https://github.com/bigskysoftware/htmx)
- [`react`](https://github.com/facebook/react)
- [`svelte`](https://github.com/sveltejs/svelte)
- [`vue`](https://github.com/vuejs/vue)

## CLI utilities

### Installation

- [Bokeh documentation](http://bokeh.pydata.org/en/latest/docs/dev_guide/setup.html) has good info on how to build, install, and mix `npm` packages with `conda`.
- Install packages using `npm install`.
- Make those packages available for the browser using [WebPack](#webpack) or [Browserify](#browserify).
  - Alternatively, you can use [Bower](#bower) instead of the _npm + WebPack_ / _Browserify_ configuration.

### Packages

#### Asset management

- WebPack (★★★) - Bundles assets (used with _npm_).
- Browserify - Similar to _WebPack_, but simpler (used with _npm_).
- Bower - Manage frontend packages (can use _npm_ + _WebPack_ / _Browserify_ instead).

### Bioinformatics

- [ngl](https://github.com/arose/ngl) - Molecular viewer. `MIT`

#### Development

- [nodemon](https://www.npmjs.com/package/nodemon)
- [light-server](https://www.npmjs.com/package/light-server)

#### Charts

- [bokeh] - Mostly for Python.
- [c3]
- [charts.js](https://github.com/chartjs/Chart.js) - Most popular. [`Canvas`]
- [`d3.js`] - Requires a lot of boilerplate for basic charts. [`SVG`]
- [highcharts] - Not free for commercial use.
- [nvd3]
- [plotly] - Large footprint.
- [victory] - Usable with ReactJS only.

**See also:**

- [Compare the Best Javascript Chart Libraries](https://blog.sicara.com/compare-best-javascript-chart-libraries-2017-89fbe8cb112d)

#### Linting

- [ESLint](http://eslint.org/) - The best linting framework. Run `eslint --init` to generate the initial config file.
- [Flow](https://github.com/facebook/flow) - Adds static typing to javascript.

Example `.eslintrc.yml` file:

```yml
extends: google
rules:
  camelcase:
    - off
  max-len:
    - warn
    - 99
```

## Web development

Basic webserver using express.js:

```javascript
var express = require("express");
var app = express();
var path = require("path");

// viewed at http://localhost:8080
app.get("/", function (req, res) {
  res.sendFile(path.join(__dirname + "/index.html"));
});

app.listen(8080);
```

## Transpilers

- CoffeeScript - Cleaner syntax than JavaScript. - Atom, etc.
- TypeScripp - Developed by MicroSoft. Type checking.
- ES6 - JavaScript with additions. Will probably develop the largest community. Use this!
