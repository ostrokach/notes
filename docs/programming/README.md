# Overview

<figure style="text-align: center">
<img src="journal.pcbi.1008549.g005.png" width="600px" ><br/>
<figcaption>Source: <a href="https://doi.org/10.1371/journal.pcbi.1008549">10.1371/journal.pcbi.1008549</a>.</figcaption>
</figure>

## Background

- [What's The Difference Between Imperative, Procedural and Structured Programming?](http://softwareengineering.stackexchange.com/q/117092/161451)

## Online coding practice

- [Exercism](http://exercism.io/) — Learn new programming languages.
- [CodeSignal](https://codesignal.com/) — Practice and certify your coding skills.
- [HackerRank](https://www.hackerrank.com/) — Solve interesting programming challenges through practice, competition and collaboration.
[LeetCode](https://leetcode.com/) — Pick from an expanding library of questions, code and submit your solution to see if you have solved it correctly.
- [TopCoder](https://www.topcoder.com/) — Register and compete in challenges.
- [Rosalind](http://rosalind.info/)
