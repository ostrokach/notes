# SVG

## Background

For our particular use-case, SVG is prefered over Canvas for making graphics on the web because:

- SVGs remain as vector graphics when the page is printed to a PDF file.
- SVG gives us a DOM which can be styled with CSS.

| Canvas                                                                                 | SVG                                                                                    |
| -------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------- |
| Pixel based (Dynamic .png)                                                             | Shape based                                                                            |
| Single HTML element                                                                    | Multiple graphical elements, which become part of the DOM                              |
| Modified through script only                                                           | Modified through script and CSS                                                        |
| Event model/user interaction is granular (x,y)                                         | Event model/user interaction is abstracted (rect, path)                                |
| Performance is better with smaller surface, a larger number of objects (>10k), or both | Performance is better with smaller number of objects (<10k), a larger surface, or both |

Table extracted from [MicroSoft - SVG vs canvas: how to choose](https://docs.microsoft.com/en-us/previous-versions/windows/internet-explorer/ie-developer/samples/gg193983(v=vs.85)?redirectedfrom=MSDN#high-level-summary-of-canvas-vs-svg).

## Frameworks
