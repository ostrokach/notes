# R

## Introduction

## Data types

- `vector`
- `list`
- `data.frame` — Deprecated. Use `tibble` instead.

## Useful functions

- `attr` — Get attribute from object.
- `attributes` — Get a list of object attributes.
- `class` — Get object class.
- `typeof` — Get object type.
