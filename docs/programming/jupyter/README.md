# Jupyter

## Execution

- [`papermill`](https://github.com/nteract/papermill) — Parameterize and execute Jupyter notebooks.

## Orchestration

- [`orchest`](https://www.orchest.io/) — Chain together multiple Jupyter notebooks in a data processing pipeline in your browser.

## Viewers

- [`commuter`](https://github.com/nteract/commuter) - Viewer for jupyter notebooks (similar to _nbviewer_). Focuses on support for cloud storage buckets.
- [`nbviewer`](https://github.com/jupyter/nbviewer) - Viewer for jupyter notebooks. To serve local notebooks using a docker image, run:<br>`docker run -p 8080:8080 -v $(realpath .):/localfiles jupyter/nbviewer python -m nbviewer --port=8080 --localfiles=/localfiles`
- [`nteract`](https://github.com/nteract/nteract) - Interactive computing suite.

## Widgets

- [`ipywidgets`](https://github.com/jupyter-widgets/ipywidgets) -
- [`ipyvuetify`](https://github.com/mariobuikhuizen/ipyvuetify) -

## Installation

### Uninstall all extensions

```bash
rm -r ~/anaconda/etc/jupyter
rm -r ~/anaconda/share/jupyter
rm -r ~/.jupyter/data/nbextensions
rm -r ~/.ipython/nbextensions/
rm -r ~/.ipython/nbextensions.bak/
rm -r ~/.ipython/extensions/
```

### Install everything

```bash
# Install jupyter
conda upgrade --force -yq jupyter

# Install nbextensions
pip install -U --no-deps -q --force https://github.com/ipython-contrib/jupyter_contrib_nbextensions/tarball/master
jupyter contrib nbextension install --sys-prefix

# Install ipywidgets
pip install -U --no-deps -q --force ipywidgets

# Install widgetsnbextension (in the base conda environment)
pip install -U --no-deps -q --force widgetsnbextension
jupyter nbextension enable --py --sys-prefix widgetsnbextension

# Install nglview (in the environment containing the ipython kernel)
pip install -U --no-deps -q --force nglview
jupyter-nbextension enable --py --sys-prefix nglview
```
