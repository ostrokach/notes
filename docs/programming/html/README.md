# HTML

- `svg` - Shape based. Saved as vector graphics in exported PDF files.
- `canvas` - Pixel-based ("dynapic png"). Single HTML element. High performance manipulation of individual pixels. Saved as raster graphics in exported PDF files. Compared to _svg_, canvas has poor text rendering capability, lack of animation, and mediocre accessibility support

## Font frameworks

- [`biocoins`](https://bioicons.com/)
- [`fontawesome`](https://fontawesome.com/)
- [`simpleicons`](https://simpleicons.org/)
- [`vscode-codicons`](https://github.com/microsoft/vscode-codicons/blob/master/dist/codicon.csv)
- [`reactome-icons`](https://reactome.org/icon-lib/)
- [`EMBL-EBI`](https://www.ebi.ac.uk/style-lab/general/fonts/v1.3/) — [`GitHub`](https://github.com/ebiwd/EBI-Icon-fonts)

## Color frameworks

- https://tailwindcolor.com/
- https://flatuicolors.com/palette/defo

## Fonts

- `FiraCode`
- `IBM Plex`
- `Inter`
- [`JetBrains Mono`](https://www.jetbrains.com/lp/mono/)
- `Roboto Mono`
- `Source Code Pro`
- `Ubuntu Mono`

https://www.programmingfonts.org/
