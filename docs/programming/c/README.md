# C

## Background

### Textbooks

- [Modern C](https://modernc.gforge.inria.fr/) — Great resource, although somewhat oppinionated. Free ebook is available.

## Compiling

### GCC

<p style="text-align: center">
<img src="static/compilation-stages.png" />
</p>

To compiline a single file with no dependencies into an executable, run:

```bash
gcc -Wall -o example example.c
```

To compile multiple files with no external dependencies into an executable, run:

```bash
gcc -Wall -o example example1.c example2.c example3.c
```

To link against external libraries, we can provice the `-l{libname}` argument _after_ the list of file names:

```bash
gcc -Wall -o example example1.c example2.c example3.c -lm
```

If we do not wish to recomplie the entire project every time we modify a single file, we can compile our code into libraries and then link against those libraries:

```bash
gcc -Wall -c square.c
gcc -Wall -o main main.c square.o -lm
```

We could even generate shared libraries and link against _those_:

```bash
gcc -c -fPIC square.c
gcc -shared -o libsquare.so square.o -lm
gcc -Wall -L. -o main main.c -lsquare
LD_LIBRARY_PATH=. ./main
```

Note that if we try running `./main` above without setting the `LD_LIBRARY_PATH` environment variable, we are likely to receive the following error:

```bash
./main: error while loading shared libraries:
  libsquare.so: cannot open shared object file: No such file or directory
```

One way to circumvent this is to provide the `-Wl,-rpath,'$ORIGIN/.'` argument when compiling our program. This will create an executable that will automatically look in the same directory as the executable for the required library files.

```bash
gcc -c -fPIC square.c
gcc -shared -o libsquare.so square.o -lm
gcc -Wall -L. -Wl,-rpath,'$ORIGIN/.'  -o main main.c -lsquare
./main
```

### Static vs. dynamic libraries

- [IBM: When to use dynamic linking and static linking](https://www.ibm.com/support/knowledgecenter/en/ssw_aix_71/performance/when_dyn_linking_static_linking.html) - Good comparison of performance characteristics of programs compiled against static or dynamic libraries.

## Standard library

- <https://en.wikipedia.org/wiki/C_standard_library>.

Manpages:

- man 2 - `posix` Posix.
- man 3 - `libc` C standard library.

### IO

**_One character at a time:_**

- [`putc`](https://man7.org/linux/man-pages/man3/puts.3.html) - Writed a character to stdout. To write a null-terminted string to stdout, use `puts`. To write to a file, use the `f...` variants.
- [`getc`](https://man7.org/linux/man-pages/man3/fgets.3.html) - Read a character from stdin.

**_Using formatting strings:_**

- [`printf`](https://man7.org/linux/man-pages/man3/printf.3.html) - Write formatted string to stdout. Use `fprintf` to write formatted string to a file.
- [`fscanf`](https://man7.org/linux/man-pages/man3/scanf.3.html) - Read a formatted string from stdin.

**_Multiple lines at a time:_**

- `fread`
- `fwrite`

**_Working with file handles:_**

- [`fopen`](https://man7.org/linux/man-pages/man3/fopen.3.html) - Opens a file.
- [`fclose`](https://man7.org/linux/man-pages/man3/fclose.3.html) - Flushes the stream and closes the file descriptor.
- [`opendir`](https://www.man7.org/linux/man-pages/man3/opendir.3.html) - Open directory stream.
- [`readdir`](https://www.man7.org/linux/man-pages/man3/readdir.3.html) - Read a directory.
- [`closedir`](https://www.man7.org/linux/man-pages/man3/closedir.3.html) - Close a directory.
- [`perror`](https://man7.org/linux/man-pages/man3/perror.3.html) - Print a system error message.

**_File status:_**

- [`stat`](https://man7.org/linux/man-pages/man2/stat.2.html) - Get file status.

```c
#include <stdio.h>

int main() {
    FILE *fp = fopen("input.txt");
    if (fp == NULL) {
        perror("input.txt");
        return 1;
    }
    fclose(fp);

    DIR *dp = *opendir(const char *name);
}
```

```c
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

int fexists(char *file) {
    struct stat statbuf;
    return(stat(file, &statbuf) == 0 && S_ISREG(statbuf.st_mode));
}
```

### Strings

- [`strcat`](https://man7.org/linux/man-pages/man3/strcat.3.html) - Concatenate two strings.
- [`strlen`](https://man7.org/linux/man-pages/man3/strlen.3.html) - Length of a string.

### File descriptors

<https://en.wikipedia.org/wiki/File_descriptor>

- [`select`](https://man7.org/linux/man-pages/man2/select.2.html) - Synchronously monitor multiple file descriptors for activity.
- [`poll`](https://man7.org/linux/man-pages/man2/poll.2.html) - Synchronously monitor multiple file descriptors for activity.
- [`epoll`](https://man7.org/linux/man-pages/man2/epoll.2.html) - Asynchronously monitor multiple file descriptors for activity. Used under the hood by [`libuv`](https://github.com/libuv/libuv), which backs Node.js and Python acync event loops.

See also:

- [Julia Evan's post: Async IO on Linux: select, poll, and epoll](https://jvns.ca/blog/2017/06/03/async-io-on-linux--select--poll--and-epoll/)
- [Eli Bendersky's post: Concurrent Servers](https://eli.thegreenplace.net/2017/concurrent-servers-part-1-introduction/)
- [Beej's Guide to Network Programming](http://beej.us/guide/bgnet/)

### Unix file system

### Sockets

- [`socket`](https://man7.org/linux/man-pages/man2/socket.2.html) - Create a network socket.
- [`bind`](https://man7.org/linux/man-pages/man2/bind.2.html) - Bind a socket to a particular IP and port.
- [`listen`](https://man7.org/linux/man-pages/man2/listen.2.html) -
- [`accept`](https://man7.org/linux/man-pages/man2/accept.2.html) -

## Data types

### struct

A collection of several attributers. The `struct fruit_order` part defines `fruit_order` in the struct "namespace" (so you can write `void foo(struct fruit_order order) {...}`), while the `typedef ... fruit_order` defines `fruit_order` in the global namespace (so you can write `void foo(fruit_order order) {...}`).

```c
#include <stdio.h>

typedef struct fruit_order {
    char* name;
    char* country;
    struct fruit_order* next;
} fruit_order;


int main(void) {
    // your code goes here
    fruit_order x;
    x.name = "hello world";
    printf("%s\n", x.name);

    fruit_order* y;
    y->name = "goodbye world";
    printf("%s\n", y->name);

    fruit_order* z;
    z->next = y;
    printf("%s\n", z->next->name);

    return 0;
}

# hello world
# goodbye world
# goodbye world
```

### union

Define a single region in memory that can be used to store data with different types. Not neccessarily used much in basic code.

```c
#include <stdio.h>

typedef union {
    short count;
    float weight;
    float volume;
} quantity;

int main() {
    quantity x;
    x.count = 10;
    printf("%i\n", x);
}
```

### enum

```c
typedef enum {
    COUNT, POUNDS, PINTS
} unit_of_measure;

int main() {
    unit_of_pressure unit = COUNT;
}
```

## Dynamic memory allocation

- malloc
- free

## Parallel computing

```c
#include <stdio.h>
#include <unistd.h>

int main() {
    pid_t pid = fork();

    if (pid == 0)
    {
        // child process
    }
    else if (pid > 0)
    {
        // parent process
    }
    else
    {
        // fork failed
        perror("fork");
        return 1;
    }
}
```

## Signals

- [`sigaction`](https://man7.org/linux/man-pages/man2/sigaction.2.html) - Examine and change a signal action.

```c
int main() {
    struct sigaction sa;
    extern void myhandler();

    sa.sa_handler = myhandler;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = 0;
    sigaction(15, &sa, NULL);
}
```

### Function pointers

```c
int (*my_int_f)(int) = &abs;
// the & operator can be omitted, but makes clear that the "address of" abs is used here
```

### Casting

- Declaration mimics use.

## Macros

```c
#DEFINE WITH_TAX(x) ((x) * 1.08)
// Parenthesis around x above are needed in order for the macro
// to be evaluated for each argument independently.

int main() {
    double purchase1 = 123.0;
    double purchase2 = 321.0;
    double total = WITH_TAX(purchase1 + purchase2);
    printf("%f\n", total);
}
```
