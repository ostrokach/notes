# Bash

## Builtin commands

- `return [n]` - The function terminates with optional status code `n` (zero by default).
- `exit [n]` - The *entire program* terminates with optional status code `n` (zero by default).

## CLI

### Commands

#### `eval`

#### `exec`

#### `file`

- Determine file type.

#### `nmap`

```bash
nmap -sP "192.168.6.*"
```

#### `nslookup`

```bash
$ nslookup 8.8.8.8
Server:         172.16.128.51
Address:        172.16.128.51#53

Non-authoritative answer:
8.8.8.8.in-addr.arpa    name = google-public-dns-a.google.com.

Authoritative answers can be found from:
8.8.8.in-addr.arpa      nameserver = ns1.google.com.
8.8.8.in-addr.arpa      nameserver = ns2.google.com.
8.8.8.in-addr.arpa      nameserver = ns4.google.com.
8.8.8.in-addr.arpa      nameserver = ns3.google.com.
ns4.google.com  internet address = 216.239.38.10
ns4.google.com  has AAAA address 2001:4860:4802:38::a
ns1.google.com  internet address = 216.239.32.10
ns1.google.com  has AAAA address 2001:4860:4802:32::a
ns2.google.com  internet address = 216.239.34.10
ns2.google.com  has AAAA address 2001:4860:4802:34::a
ns3.google.com  internet address = 216.239.36.10
ns3.google.com  has AAAA address 2001:4860:4802:36::a
```

#### `realpath`

```bash
$ realpath --help
Usage: realpath [OPTION]... FILE...
Print the resolved absolute file name;
all but the last component must exist

  -e, --canonicalize-existing  all components of the path must exist
  -m, --canonicalize-missing   no path components need exist or be a directory
  -L, --logical                resolve '..' components before symlinks
  -P, --physical               resolve symlinks as encountered (default)
  -q, --quiet                  suppress most error messages
      --relative-to=FILE       print the resolved path relative to FILE
      --relative-base=FILE     print absolute paths unless paths below FILE
  -s, --strip, --no-symlinks   don't expand symlinks
  -z, --zero                   end each output line with NUL, not newline

      --help     display this help and exit
      --version  output version information and exit

GNU coreutils online help: <http://www.gnu.org/software/coreutils/>
Full documentation at: <http://www.gnu.org/software/coreutils/realpath>
or available locally via: info '(coreutils) realpath invocation'
```

### Print bash function source code

```bash
myfun () {
    echo "Hello world!"
}

type myfun
# myfun is a function
# myfun ()
# {
#     echo "Hello world!"
# }

declare -f myfun
# myfun ()
# {
#     echo "Hello world!"
# }
```

### Process Substitution

https://www.gnu.org/software/bash/manual/bashref.html#Process-Substitution

Process substitution allows a process’s input or output to be referred to using a filename. It takes the form of

```bash
<(list)
```

or

```bash
>(list)
```

The process list is run asynchronously, and its input or output appears as a filename. This filename is passed as an argument to the current command as the result of the expansion. If the `>(list)` form is used, writing to the file will provide input for list. If the `<(list)` form is used, the file passed as an argument should be read to obtain the output of list. Note that no space may appear between the < or > and the left parenthesis, otherwise the construct would be interpreted as a redirection. Process substitution is supported on systems that support named pipes (FIFOs) or the `/dev/fd` method of naming open files.

When available, process substitution is performed simultaneously with parameter and variable expansion, command substitution, and arithmetic expansion.
