# Awesome Software

Useful packages written in any framework / language. For Python-specific packages, see [Python](../python).

## Big data

- [`forklift`](http://www.exmachinatech.net/projects/forqlift/download/) — Toolkit for working with Hadoop Sequence files.

## Configuration

- [`jsonnet`](https://github.com/google/jsonnet) → Configuration language similar to GCL. For a faster implementation written in Go, see: [`go-jsonnet`](https://github.com/google/go-jsonnet).

## Containers

- [Podman](https://podman.io/) — Podman is a daemonless container engine for developing, managing, and running OCI Containers on your Linux System. Containers can either be run as root or in **rootless mode**. Simply put: `alias docker=podman`.
- [Singularity](https://sylabs.io/) — Singularity is a container solution with a focus on building reproducible software stacks and running them most efficiently on existing HPC, scientific, compute farm and even enterprise architectures.
- [Docker](https://www.docker.com/) — Docker makes deploying your entire development environment easier and portable than many other container software.

## Data storage

- [iRods](https://irods.org/) — Open Source Data Management Software. ([Source Code](https://github.com/irods/irods)) `BSD`

## Data pipelines

- [Pachyderm](http://pachyderm.io/) — A Containerized, Version-Controlled Data Lake.
- Apache Beam
- AWS Glue
- Google Cloud Dataflow

### Developer tools

- Pagerduty
- Sentry

## Shell

- [`ag`](https://github.com/ggreer/the_silver_searcher) — A code-searching tool similar to ack, but faster.
- [`fwupdmgr`](https://github.com/fwupd/fwupd) — CLI tool for getting firmware updates.
- [`mitmproxy`](https://github.com/mitmproxy/mitmproxy) — An interactive TLS-capable intercepting HTTP proxy for penetration testers and software developers.
- [`sshuttle`](https://github.com/sshuttle/sshuttle) — Transparent proxy server that works as a poor man's VPN.
- [`xpra`](https://xpra.org/) - Screen for X11.

## MLOps

- [`Argo`](https://argoproj.github.io/) — Argo Workflows is an open source container-native workflow engine for orchestrating parallel jobs on Kubernetes. Argo Workflows is implemented as a Kubernetes CRD.
- [`GitLab CI`](https://docs.gitlab.com/runner/executors/kubernetes.html) — Kubernetes executor currently ignores the Dockerfile entrypoint (see [`gitlab-org/gitlab-runner#4125`](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/4125)).
- [`Kubeflow`](https://www.kubeflow.org/) — Kubeflow is a free and open-source machine learning platform designed to enable using machine learning pipelines to orchestr. Sponsored by Google. Kubeflow pipelines builds on top of Argo.
- [`Metaflow`](https://metaflow.org/) — AWS-centric. Can deploy to Kubernetes (powered by Argo Workflows).
- [`MLflow`](https://mlflow.org/) — An open source platform for the machine learning lifecycle. Sponsored by Databricks and Microsoft. MLflow pipelines build on top of the Kubernetes Jobs API. Has more mature experiment tracking than `Kubeflow`.
- [`Ploomber`](https://github.com/ploomber/ploomber) — The fastest way to build data pipelines. Develop iteratively, deploy anywhere (Kubernetes, Airflow, AWS Batch, and SLURM).
- [`Tecton`](https://www.tecton.ai/feature-store/) — Feature store for enterprise data science workloads.
- [`Tekton`](https://tekton.dev/) — Tekton is a powerful and flexible open-source framework for creating CI/CD systems, allowing developers to build, test, and deploy across cloud providers and on-premise systems.

**Additional resources:**

- [Best Workflow and Pipeline Orchestration Tools: Machine Learning Guide](https://neptune.ai/blog/best-workflow-and-pipeline-orchestration-tools)
- [Open-source Workflow Management Tools: A Survey](https://ploomber.io/blog/survey/)

## Web

- [`h5ai`](https://larsjung.de/h5ai/) - Modern HTTP web server index.
- [`monolith`](https://github.com/Y2Z/monolith) - Save entire web page as a single html document.
- `http-prompt`
- `httpie`
- `mycli` — MySQL CLI.
- `ngrok` —

## Web automation

- [`selenium`](https://www.selenium.dev/) — Selenium automates the browser. Includes Selenium WebDriver, a collection of language specific bindings to drive a browser.
- [`puppeteer`](https://github.com/puppeteer/puppeteer) — Provides APIs for automating the Chromium browser.
- [`pypeteer`](https://github.com/pyppeteer/pyppeteer) — Headless chrome/chromium automation library (unofficial port of puppeteer).
- [`playwright`](https://playwright.dev/) — Playwright enables reliable end-to-end testing for modern web apps.
- [`playwright-python`](https://github.com/microsoft/playwright-python) — Python version of the Playwright testing and automation library. [[docs](https://playwright.dev/python/)]

