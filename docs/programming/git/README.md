# Git

## Git commands

- [git-tips](https://github.com/git-tips/tips) - ... `Awesome`

#### Difference between two consecutive commits to the same branch:

```bash
git diff --name-only HEAD HEAD~1
```

## Git Hosting

- [`gogs`](https://github.com/gogits/gogs) — Gogs (Go Git Service) is a painless self-hosted Git service ([Website](https://gogs.io)). [`GO`] <br> ![](https://img.shields.io/github/stars/gogits/gogs.svg?style=social)

- [`gitlab`](https://github.com/gitlabhq/gitlabhq) — [`Ruby`] <br> ![](https://img.shields.io/github/stars/gitlabhq/gitlabhq.svg?style=social)

- [`gitbucket`](https://github.com/gitbucket/gitbucket) — A Git platform powered by Scala with easy installation, high extensibility & github API compatibility [Website](https://gitbucket.github.io). [`Scala`] <br> ![](https://img.shields.io/github/stars/gitbucket/gitbucket.svg?style=social)

- [`gitblit`](https://github.com/gitblit/gitblit) — Pure java git solution ([Website](http://gitblit.com)). [`Java`] <br>![](https://img.shields.io/github/stars/gitblit/gitblit.svg?style=social)
