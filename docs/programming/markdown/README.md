# Markdown

See the [CommonMark](http://commonmark.org/) spec.

Comparison of markdown engines: <http://johnmacfarlane.net/babelmark2>.

## Markdown engines

### JavaScript

- [`kramed`](https://github.com/GitbookIO/kramed) → A markdown (kramdown compatible) parser and compiler. Built for speed. (Fork of marked). <br> ![stars](https://img.shields.io/github/stars/GitbookIO/kramed.svg?style=social&logo=github)
- [`markdown-it`](https://github.com/markdown-it/markdown-it) → Markdown parser, done right. 100% CommonMark support, extensions, syntax plugins & high speed. <br> ![stars](https://img.shields.io/github/stars/markdown-it/markdown-it.svg?style=social&logo=github)
- [`markdown-js`](https://github.com/evilstreak/markdown-js) → A Markdown parser for javascript. <br> ![stars](https://img.shields.io/github/stars/evilstreak/markdown-js.svg?style=social&logo=github)
- [`Markdown+Math`](https://marketplace.visualstudio.com/items?itemName=goessner.mdmath) → VSCode extension for writing documents with math (including equation numbers). Uses `markdown-it`. <br> ![stars](https://img.shields.io/github/stars/goessner/mdmath.svg?style=social&logo=github)
- [`marked`](https://github.com/chjj/marked) → A markdown parser and compiler. Built for speed. <br>![stars](https://img.shields.io/github/stars/chjj/marked.svg?style=social&logo=github)
- [`pandoc-url2cite`](https://github.com/phiresky/pandoc-url2cite) → A way to convert URLs to citations inside documents. <br> ![stars](https://img.shields.io/github/stars/phiresky/pandoc-url2cite.svg?style=social&logo=github)
- [`remark`](https://github.com/wooorm/remark) → Markdown processor powered by plugins. <br> ![stars](https://img.shields.io/github/stars/wooorm/remark.svg?style=social&logo=github)

### Python

- [`CommonMark-py`](https://github.com/rtfd/CommonMark-py) → Markdown parser compliant with the CommonMark spec. <br> ![stars](https://img.shields.io/github/stars/rtfd/CommonMark-py.svg?style=social&logo=github)
- [`Docutils`](https://docutils.sourceforge.io/) → Utilities for general- and special-purpose documentation. Includes reStructuredText, the easy to read, easy to use, what-you-see-is-what-you-get plaintext markup language.
- [`markdown-it-py`](https://github.com/executablebooks/markdown-it-py) → Markdown parser, done right. 100% CommonMark support, extensions, syntax plugins & high speed. Now in Python! <br> ![stars](https://img.shields.io/github/stars/executablebooks/markdown-it-py.svg?style=social&logo=github)
- [`misaka`](https://github.com/FSX/misaka) → A Python binding for Hoedown. <http://misaka.61924.nl> <br> ![stars](https://img.shields.io/github/stars/FSX/misaka.svg?style=social&logo=github)
- [`mistune`](https://github.com/lepture/mistune) → The fastest markdown parser in pure Python with renderer feature. Used by [nbconvert](https://github.com/jupyter/nbconvert). [[Docs](http://mistune.readthedocs.io/)] <br> ![stars](https://img.shields.io/github/stars/lepture/mistune.svg?style=social&logo=github)
- [`MyST-Parser`](https://github.com/executablebooks/MyST-Parser) ★ → An extended commonmark compliant parser, with bridges to docutils/sphinx. Use by the [executablebooks](https://executablebooks.org/) project. <br> ![stars](https://img.shields.io/github/stars/executablebooks/MyST-Parser?style=social&logo=github)
- [`Python-Markdown`](https://github.com/waylan/Python-Markdown) ★ → Used by [mkdocs](https://github.com/mkdocs/mkdocs) and [Django-Wiki](#django-wiki). <br> ![stars](https://img.shields.io/github/stars/Python-Markdown/markdown.svg?style=social&logo=github)
- [`Python-Markdown2`](https://github.com/trentm/python-markdown2) → Apparently this is slower than MarkDown. No reason to use this? <br> ![stars](https://img.shields.io/github/stars/trentm/python-markdown2.svg?style=social&logo=github)
- [`recommonmark`](https://github.com/rtfd/recommonmark) → A docutils-compatibility bridge to CommonMark. <br> ![stars](https://img.shields.io/github/stars/rtfd/recommonmark.svg?style=social&logo=github)

See also: [Comparison of MarkDown engines](http://lepture.com/en/2014/markdown-parsers-in-python).

### C

- [`hoedown`](https://github.com/hoedown/hoedown) →  <br> ![stars](https://img.shields.io/github/stars/hoedown/hoedown.svg?style=social&logo=github)
