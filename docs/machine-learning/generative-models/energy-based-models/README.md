# Energy-based models

- [NYU DS-GA 1008 Week 7 – Lecture: Energy based models and self-supervised learning](https://www.youtube.com/watch?v=tVwV14YkbYs)

```math
\hat{y} = \argmin_{y} E(x, y)
```
