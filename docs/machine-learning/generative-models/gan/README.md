# Generative Adverserial Networks

Have two networks: a generative network `$G(z)$`, which tries to generate images which look like true images, starting from a vector of random numbers `$z$`, and a discriminator network, `$D(Y)$`.

**See also:**

- [What are some recent and potentially upcoming breakthroughs in deep learning?](https://www.quora.com/What-are-some-recent-and-potentially-upcoming-breakthroughs-in-deep-learning)
- [wiseodd/generative-models](https://github.com/wiseodd/generative-models)
- [GANs, a modern perspective](https://medium.com/deep-dimension/gans-a-modern-perspective-83ed64b42f5c)

## Background

### Mode collapse

> A commonly encountered failure case for GANs where the generator learns to produce samples with extremely low variety.

### Deconvolutions

- [Deconvolution and Checkerboard Artifacts](https://distill.pub/2016/deconv-checkerboard/)

## Example architectures

#### DCGAN

- https://github.com/Newmu/dcgan_code (theano)
- https://github.com/carpedm20/DCGAN-tensorflow

#### pix2pix

- https://github.com/phillipi/pix2pix
- [Image-to-Image Translation in Tensorflow](https://affinelayer.com/pix2pix/)
- [Image-to-Image Translation with Conditional Adversarial Networks](https://arxiv.org/abs/1611.07004)

#### WassersteinGAN

- https://github.com/martinarjovsky/WassersteinGAN

## Bibliography

- [Progressive Growing of GANs for Improved Quality, Stability, and Variation](https://arxiv.org/abs/1710.10196)
