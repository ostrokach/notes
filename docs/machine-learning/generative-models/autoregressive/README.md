# Auto-regressive Generative Models

## Examples

- `PixelRNN` — Recurrent NN which predicts pixel categories given the previous pixels.
- `PixelCNN` — Output pixels are represented as 256 categorical variables. Use a masked convolution to include only preceding pixel information. Faster but lower accuracy than `PixelRNN`.
- `PixelCNN++` — Instead of representing output pixels as categorical variables, represent output pixels as a set of distributions parameterized by parameters produced by the network.

## Additional resources

- [Auto-Regressive Generative Models (PixelRNN, PixelCNN++)](https://towardsdatascience.com/auto-regressive-generative-models-pixelrnn-pixelcnn-32d192911173)
- [Bounded Rationality - PixelCNN](http://bjlkeng.github.io/posts/pixelcnn/)
- [Bounded Rationality - Autoregressive Autoencoders](http://bjlkeng.github.io/posts/autoregressive-autoencoders/)