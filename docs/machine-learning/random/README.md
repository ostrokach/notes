# Random

## Books

- [Notes on Artificial Intelligence](https://github.com/frnsys/ai_notes) — Dirichlet process, Gaussian process, Bayes stuff, lots of good stuff... <br/>![](https://img.shields.io/github/stars/frnsys/ai_notes.svg?style=flat-square)

## Hyperparameter optimization

- [Random Search for Hyper-Parameter Optimization](http://www.jmlr.org/papers/volume13/bergstra12a/bergstra12a.pdf) - Random search does better than gridsearch for hyperparameter optimization.
- [Automating Model Search for Large Scale Machine Learning](https://amplab.cs.berkeley.edu/wp-content/uploads/2015/07/163-sparks.pdf) - Hyperopt is good. Random search is not far behind.
- [Hyperband: A Novel Bandit-Based Approach to Hyperparameter Optimization](https://arxiv.org/pdf/1603.06560.pdf) - Random search does as well as hyperparameter optimization.
- [The News on Auto-tuning](http://www.argmin.net/2016/06/20/hypertuning/)
- [Embracing the Random](http://www.argmin.net/2016/06/23/hyperband/)

## Kalman filters

- [Kalman-and-Bayesian-Filters-in-Python](https://github.com/rlabbe/Kalman-and-Bayesian-Filters-in-Python) — Kalman Filter book using Jupyter Notebook. Focuses on building intuition and experience, not formal proofs. Includes Kalman filters, extended Kalman filters, unscented Kalman filters, particle filters, and more. All exercises include solutions.<br/>![](https://img.shields.io/github/stars/rlabbe/Kalman-and-Bayesian-Filters-in-Python.svg?style=flat-square)

- [`filterpy`](https://github.com/rlabbe/filterpy) — Kalman filtering and optimal estimation library in Python. Kalman filter, Extended Kalman filter, Unscented Kalman filter, g-h, least squares, H Infinity, smoothers, and more. Has companion book 'Kalman and Bayesian Filters in Python'.<br/>![](https://img.shields.io/github/stars/rlabbe/filterpy.svg?style=flat-square)

## Particle filters

Better than Kalman filters but more difficult to implement.

## Markov random field

```math
p(y, x)
```

Probability distribution over random variables.

### Conditional random fields

```math
p(y \mid x)
```

Always involve inputs and outputs.

> A generalization of logistic regression to have structured outputs, like chains, trees, or grids.

## Detrimental point processes

Can be used to increase the level of diversity in the samples that we generate. Other options are importance sampling, etc.

Alex Kulesza and Ben Taskar (2012) _Determinantal point processes for machine learning_ https://arxiv.org/abs/1207.6083

## Latent dirichlet allocation

- Select subset of words that are most indicative of a particular topic.
- Implemented in e.g., [`scikit-learn`](http://scikit-learn.org/stable/modules/generated/sklearn.decomposition.LatentDirichletAllocation.html), [`gensim`](https://radimrehurek.com/gensim/models/ldamodel.html).

## Non-negative matrix factorization
