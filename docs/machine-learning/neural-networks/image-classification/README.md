# Image classification

<https://www.paperswithcode.com/task/image-classification>

## Models

- [`VGG`](https://arxiv.org/abs/1409.1556v6) — *Very Deep Convolutional Networks for Large-Scale Image Recognition* (2014)<br>Convolutions, ReLU, max-pooling.
- [`Inception`](https://doi.org/10.1109/CVPR.2015.7298594) — *Going deeper with convolutions* (2015)<br>Introduced in the original GoogLeNet paper. Many variants have been developed by the same authors.
- [`ResNet`](https://arxiv.org/abs/1512.03385v1) — *Deep Residual Learning for Image Recognition* (2015)
- [`EfficientNet`](http://arxiv.org/abs/1905.11946) — *EfficientNet: Rethinking Model Scaling for Convolutional Neural Networks* (2019) [[`code`](https://github.com/tensorflow/tpu/tree/master/models/official/efficientnet)]<br>Do more hyparameter tuning on the `Inception` model to optimize both classification accuracy and speed. Many variants have been developed, such as noisy student and [sharpness-aware minimization (SAM)](https://arxiv.org/abs/2010.01412v2) ([`code`](https://github.com/google-research/sam)). 
- [`BiT`](https://arxiv.org/abs/1912.11370v3) — *Big Transfer (BiT): General Visual Representation Learning* (2019)
- [`ViT`](https://arxiv.org/abs/2010.11929v1) — *An Image is Worth 16x16 Words: Transformers for Image Recognition at Scale* (2020)<br>Attention is all you need (image classification edition).
