# Geometric Deep Learning

## Resources

- <https://geometricdeeplearning.com/>
- <https://geometricdeeplearning.com/lectures/>
- <https://gm-neurips-2020.github.io/>

## Finding graph structure from data

- Halcrow et al. (2020) _Grale: Designing Networks for Graph Learning_ https://arxiv.org/abs/2007.12002
