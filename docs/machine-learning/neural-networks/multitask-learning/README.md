# Combining multiple datasets

## Multitask learning

- [The Benefit of Multitask Representation Learning](http://www.jmlr.org/papers/volume17/15-242/15-242.pdf)
- [Massively Multitask Networks for Drug Discovery](https://arxiv.org/pdf/1502.02072.pdf)

## Transfer learning

- [Practical Lessons from Predicting Clicks on Ads at Facebook](https://ostrokach.gitlab.io/notes-private/files/adkdd_2014_camera_ready_junfeng.pdf)
