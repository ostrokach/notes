# Neural Networks

## Courses

<https://github.com/Machine-Learning-Tokyo/AI_Curriculum>

### MIT — MIT 6.S191: Introduction to Deep Learning

<http://introtodeeplearning.com/>

[Playlist](https://www.youtube.com/playlist?list=PLtBw6njQRU-rwp5__7C0oIVt26ZgjG9NI)

### NYU — DS-GA 1008: Deep Learning

<https://atcold.github.io/pytorch-Deep-Learning/>

[Playlist](https://www.youtube.com/playlist?list=PLLHTzKZzVU9eaEyErdV26ikyolxOsz6mq)

### Stanford — CS231n: Convolutional Neural Networks for Visual Recognition

<http://cs231n.stanford.edu/>

- [Lecture 1 | Introduction to Convolutional Neural Networks for Visual Recognition](https://www.youtube.com/watch?v=vT1JzLTH4G4)
- [Lecture 2 | Image Classification](https://www.youtube.com/watch?v=OoUX-nOEjG0)
- [Lecture 3 | Loss Functions and Optimization](https://www.youtube.com/watch?v=h7iBpEHGVNc)
- [Lecture 4 | Introduction to Neural Networks](https://www.youtube.com/watch?v=d14TUNcbn1k)
- [Lecture 5 | Convolutional Neural Networks](https://www.youtube.com/watch?v=bNb2fEVKeEo)
- [Lecture 6 | Training Neural Networks I](https://www.youtube.com/watch?v=wEoyxE0GP2M)
- [Lecture 7 | Training Neural Networks II](https://www.youtube.com/watch?v=_JB0AO7QxSA)
- [Lecture 8 | Deep Learning Software](https://www.youtube.com/watch?v=6SlgtELqOWc)
- [Lecture 9 | CNN Architectures](https://www.youtube.com/watch?v=DAOcjicFr1Y)
- [Lecture 10 | Recurrent Neural Networks](https://www.youtube.com/watch?v=6niqTuYFZLQ)
- [Lecture 11 | Detection and Segmentation](https://www.youtube.com/watch?v=nDPWywWRIRo)
- [Lecture 12 | Visualizing and Understanding](https://www.youtube.com/watch?v=6wcs6szJWMY)
- [Lecture 13 | Generative Models](https://www.youtube.com/watch?v=5WoItGTWV54&t=612s)
- [Lecture 14 | Deep Reinforcement Learning](https://www.youtube.com/watch?v=lvoHnicueoE)

### Stanford — CS236: Deep Generative Models

<https://deepgenerativemodels.github.io/>

Video recordings are not available but use the syllabus for links to slides.

### University of Toronto — CSC2547: Methods in 3D and Geometric Deep Learning

<https://www.pair.toronto.edu/csc2547-w21/schedule/>

[Playlist](https://www.youtube.com/channel/UCrsmAXnwu6sgccWevW12Dfg)

### Other

- [DEEP LEARNING COURSE OF UNIGE/EPFL`](https://fleuret.org/dlc/)
- [Hugo Larochelle](https://www.youtube.com/playlist?list=PL6Xpj9I5qXYEcOhn7TqghAJ6NAPrNmUBH)

## Convolution arithmetic

https://github.com/vdumoulin/conv_arithmetic

## Batch / instance normalization

- [Notes for “Batch Normalization: Accelerating Deep Network Training by Reducing Internal Covariate Shift” paper](https://gist.github.com/shagunsodhani/4441216a298df0fe6ab0)
- [Choosing Instance normalization instead of Batch normalization](https://github.com/mingyuliutw/UNIT/issues/15)
- [Instance Normalization: The Missing Ingredient for Fast Stylization](https://arxiv.org/abs/1607.08022)
- [Instance Normalisation vs Batch normalisation](https://stackoverflow.com/a/48118940/2063031)

### Alternatives

- [Batch Renormalization: Towards Reducing Minibatch Dependence in Batch-Normalized Models](https://arxiv.org/abs/1702.03275)
- [Weight Normalization: A Simple Reparameterization to Accelerate Training of Deep Neural Networks](https://arxiv.org/abs/1602.07868)
- [Cosine Normalization: Using Cosine Similarity Instead of Dot Product in Neural Networks](https://arxiv.org/abs/1702.05870)

## Network architectures

- [Squeeze-and-Excitation Networks](https://arxiv.org/abs/1709.01507) — Aggregate over the image dimensions, pass the resulting channel values through a feed-forward network, and use the result to scale the original inputs. [[Blog post](https://towardsdatascience.com/squeeze-and-excitation-networks-9ef5e71eacd7)]
