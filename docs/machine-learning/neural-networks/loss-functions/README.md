# Loss Functions

## Penalized tanh

```math
f(z)= \begin{cases} \tanh (x) & x>0 \\ 0.25\tanh (x) & x\leq 0\\ \end{cases}
```
