# Data preparation

## Crowd sourcing

**See also:**

- [The Practice of Crowdsourcing](https://doi.org/10.2200/S00904ED1V01Y201903ICR066) — Describes how to crowdsource data efficiently.
