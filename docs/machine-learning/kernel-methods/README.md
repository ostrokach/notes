# Kernel Methods

## Resources

### Gaussian Processes for Machine Learning

Carl Edward Rasmussen and Christopher K. I. Williams. The MIT Press, 2006. ISBN 0-262-18253-X.

- http://www.gaussianprocess.org/gpml/
- http://www.gaussianprocess.org/gpml/chapters/
- [RW](http://www.gaussianprocess.org/gpml/chapters/RW.pdf)

> Roughly speaking a stochastic process is a generalization of a probability distribution (which describes a finite-dimensional random variable) to functions.

A local mirror of the textbook can be accessed [here](http://strokach.noip.me/textbooks/machine_learning/gpml/chapters/RW.pdf).

### Arthur Gretton (Gatsby unit)

- [Introduction to Machine Learning, Kernel Methods](http://www.gatsby.ucl.ac.uk/~gretton/adaptModel/adaptModel.html)
- [Reproducing kernel Hilbert spaces in Machine Learning](http://www.gatsby.ucl.ac.uk/~gretton/coursefiles/rkhscourse.html)

### mlss2011

- mlss2011.comp.nus.edu.sg
- [A Tutorial on Gaussian Processes (or why I don’t use SVMs)](http://mlss2011.comp.nus.edu.sg/uploads/Site/lect1gp.pdf)

### Gaussian Process Latent Variable Model

- CS236 (Stanford) - Deep Generative Models - https://deepgenerativemodels.github.io/notes/.
- CSC412 (UofT) - [Tutorial: Stochastic Variational Inference](http://www.cs.toronto.edu/~madras/presentations/svi-tutorial.pdf)
