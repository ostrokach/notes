# Reinforcement learning

## Resources

- <http://rll.berkeley.edu/deeprlcourse/>

## Algorithms

### Policy gradients (REINFORCE)

<https://csc413-2020.github.io/assets/slides/lec10.pdf>

<https://www.davidsilver.uk/wp-content/uploads/2020/03/pg.pdf>

Repeat forever:

```math
\text{Sample a rollout: } \tau = (s_0, a_0, s_1, a_1,..., s_T) \\
r(\tau) \leftarrow \sum_{k=0}^T r(s_k, a_k) \\
\text{For } t = 0, ..., T: \theta \leftarrow \theta + r(\tau) \frac{\partial}{\partial \theta} \log \pi_{\theta}(a_t, s_t)
```

**Notes:**

- On-policy (~can only use data generated using the current policy).
- Works with large and continuous action spaces.
- Works with stochastic policies.

### Q-learning

<https://csc413-2020.github.io/assets/slides/lec11.pdf>

<https://www.davidsilver.uk/wp-content/uploads/2020/03/FA.pdf>

Train a neural network representing your $`Q(s_t, a_t)`$ function.

```math
Q(s_t, a_t) \leftarrow Q(s_t, a_t) + \alpha \big[ r(s_t, a_t) + \gamma \max_a Q(s_{t+1}, a) - Q(s_t, a_t)\big]
```

In the case of deep Q-learning, update the parameters of your model.

```math
\mathbf{\theta} \leftarrow \mathbf{\theta} + \alpha \big[ r(s_t, a_t) + \gamma \max_a Q(s_{t+1}, a) - Q(s_t, a_t)\big] \frac{\partial Q}{\partial \theta}
```

**Notes:**

- Off-policy (~can use data generated from previous runs using different policies; more sample efficient).
- Does credit assignment (increases the reward associated with a specific good action rather than the entire policy).
- Don't always take the best action during training (e.g. $`\epsilon$`-greedy policy).
