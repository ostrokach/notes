# Graph Neural Networks

## Datasets

- [Open graph database](https://ogb.stanford.edu)

## Probabilistic graphical models

### Courses

- [Probabilistic Graphical Models 1: Representation](https://www.coursera.org/learn/probabilistic-graphical-models)
- [Probabilistic Graphical Models 2: Inference](https://www.coursera.org/learn/probabilistic-graphical-models-2-inference)
- [Probabilistic Graphical Models 3: Learning](https://www.coursera.org/learn/probabilistic-graphical-models-3-learning)
