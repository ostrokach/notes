# Overview

<pre align="center">
sysvint -(superceded by)-> upstard -(superceded by)-> systemd
</pre>

- [Awesome Selfhosted](https://github.com/Kickball/awesome-selfhosted)
- [Awesome Sysadmin](https://github.com/kahun/awesome-sysadmin)

## SSH

- [Transparent Multi-hop SSH](http://sshmenu.sourceforge.net/articles/transparent-mulithop.html)

#### Tunnelling

```bash
ssh -f -N -L port1:server1:port2 server2
```

- `-f` - Run in background.
- `-N` - Don't execute commands.
- `-L` - Forward ports.

## Fstab

- [Introduction to fstab](https://help.ubuntu.com/community/Fstab)


## Network

- [nmap](https://nmap.org/) - Find all computers on your local network, and much more. `GPLv2`
- BCC - Tools for BPF-based Linux IO analysis, networking, monitoring, and more. ([Source Code](https://github.com/iovisor/bcc)) `C++ / Python`
- [FlameGraph](https://github.com/brendangregg/FlameGraph) -
- ag
- sshuttle
- mitmproxy
- [pythonpy](https://github.com/Russell91/pythonpy)

## Shell

- click
- [dataset](https://dataset.readthedocs.io/en/latest/)

## Process states

### Uninterruptible sleep (`D`)

- One way to kill uninterruptible processes may be to [flush linux VM cache](https://stackoverflow.com/a/45718579/2063031).
- Kernel developers should implement [`TASK_KILLABLE`](https://lwn.net/Articles/288056/).
