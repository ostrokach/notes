# Monitorning

## SAS

- [Periscope Data](https://www.periscopedata.com/) - From SQL Query to Analysis in Seconds

## Demons

- [`cacti`](http://www.cacti.net)
- [`earlyoom`](https://github.com/rfjakob/earlyoom) ★ - Kills memory hogs when user-defined memory limites are exceeded. `C`
- [`ganglia`](http://ganglia.sourceforge.net/) - Old-school tool. Easy to install. ([Source Code](https://github.com/ganglia/monitor-core)) `C`
- [`glances`](https://nicolargo.github.io/glances/) - Shell and web overview of the load on the machine. ([Source Code](https://github.com/nicolargo/glances)) `LGPL`
- [`graphana`](https://github.com/grafana/grafana) ★ - Dashboard issues queries against a database server. Looks good, but for databases only? ([Web Site](https://grafana.com/))
- [`graphite`](https://github.com/graphite-project/graphite-web) - Both a database and a simple dashboard.
- [`monit`](https://mmonit.com/monit/) -
- [`monitorix`](http://www.monitorix.org/) - Easy to install. Good design. ([Source Code](https://github.com/mikaku/Monitorix)) `Perl`
- [`nagios`](https://www.nagios.org/) -
- [`netdata`](https://github.com/firehol/netdata/) ★ - Totally awesome - ([Website](https://my-netdata.io/)) `C / Python / JavaScript`
- [`prometheus`](https://github.com/prometheus/prometheus) ★ - ([Website](https://prometheus.io/)) `GO`
- [`zabbix`](http://www.zabbix.com/)

## Tools

## Server

- [`collectl`](http://collectl.sourceforge.net/)
- [`iostat`]
- [`nfsiostat`](https://linux.die.net/man/8/nfsiostat)
- [`nfsstat`](https://linux.die.net/man/8/nfsstat)
- [`psutil`](https://github.com/giampaolo/psutil) - A cross-platform process and system utilities module for Python.

## Network

- [iperf3](https://iperf.fr/) - Measure network bandwidth between server and client. ([Source Code](https://github.com/esnet/iperf))
- [iftop](http://www.ex-parrot.com/pdw/iftop/) - iftop: display bandwidth usage on an interface.
- [nethogs](https://github.com/raboof/nethogs)
- [netstat -ptu](http://serverfault.com/a/316704/241418) - Monitor network traffic.

## I/O
