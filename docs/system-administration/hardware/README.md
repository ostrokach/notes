# Hardware

# Buying

- [Super Micro distributors](http://www.supermicro.com/wheretobuy/namerica.cfm?rgn=103&typ=DS)
- [ThinkMate](http://www.thinkmate.com/contact)
- [Source Code](http://www.sourcecode.com/system/superchassis-837e26-rjbod1)
- [Nexan](http://www.nexsan.com/)

## Storage

### Desktop

- HGST Deskstar 7K2000 (HDS722020ALA330)
- [Hot swap tray](https://www.newegg.ca/Product/Product.aspx?Item=N82E16816132037)

### Server

- [Overview of SAS technology]
- [Economical way to get many harddrives into rack mount?](http://serverfault.com/questions/220112/economical-way-to-get-many-harddrives-into-rack-mount)
- [Getting started with storage. Understanding SAN vs NAS vs DAS.](https://vanillavideo.com/blog/2014/started-storage-understanding-san-nas-das)

MD1200 are just boxes that you fill with hard drives and attach to a PowerEdge server with a PERC H800 card (or MD3200 for SAN).
