# Networking

## General

### URLs for checking your public IP address

- <https://ifconfig.me/>
- <https://ip.bsd-unix.net/>

## Web servers

### NGINX

Note that, by default, NGINX will use the first available `server` to serve requests that don't exactly match any specified server.

### TLS

Use [`certbot`](https://certbot.eff.org/) to automatically generate TLS certificates for the websites. In the case of wild-card subdomains, use [`acme-dns`](https://github.com/joohoi/acme-dns) and [`acme-dns-client`](https://github.com/acme-dns/acme-dns-client) to host the DNS `TXT` records for each of the subdomains, making it possible to modify those DNS records programmatically during ACME challenges. On the DNS provider's website, add a `CNAME` record for the `_acme-challenge.` subdomain to point to the URL hosting the `acme-dns` server.

In the case of the `*.sub.example.org` subdomain, we could run the following:

```bash
sudo acme-dns-client register -d sub.example.org -s https://auth.acme-dns.io --dangerous
sudo certbot certonly --manual --preferred-challenges dns --manual-auth-hook 'acme-dns-client' -d *.sub.example.org
sudo certbot renew --dry-run
```

## Routing

### Install on x86 systems

- [`pfSense`](https://www.pfsense.org/) — pfSense is a free and open source firewall and router that also features unified threat management, load balancing, multi WAN, and more. Designed for higher-end hardware (x86).
- [`OPNsense`](https://opnsense.org/) — Fork of `pfSense`. Has support for WireGuard.
- [`VyOS`](https://vyos.io/)

**Note:** Can also set up a Raspberry Pi as a wireless gateway.

### Install on routers

- [`OpenWRT`](https://openwrt.org/) — _OpenWRT offers even more fine-grained control than DD-WRT, but that also comes at the price of simplicity. This firmware requires some knowledge to use properly and quite a bit more to make it worthwhile. OpenWRT is best for more technical people who know exactly what they want._
- [`Tomato`](https://www.polarcloud.com/tomato) — Supports relatively few devices
- [`FreshTomato`](https://freshtomato.org/) — Supports relatively few devices. For Broadcom based routers only.
- [`DD-WRT`](https://dd-wrt.com/) — Based on `OpenWRT`.

> For most people, one of these three will support the device you have and give you all of the features you could possibly need. Of the three, DD-WRT is the most well-supported on home routers, and it’s relatively easy to install and set up. Tomato is the most user-friendly, and certainly the most attractive of the three, but it supports the fewest devices. OpenWRT supports the most gear in general, including stuff only a network engineer would use, but its learning curve can be steep. It’s also the most moddable and tweakable, and a good option if you have hardware that’s not supported by anything else. Even so, they’re not your only options by a long shot. [[source](https://www.lifehacker.com.au/2015/04/how-to-choose-the-best-firmware-to-supercharge-your-wi-fi-router/)]

**See also:**

- [Comparison of router software projects](https://en.wikipedia.org/wiki/Comparison_of_router_software_projects).

## VPN

### OpenVPN

1. Save your password to a file `my-password.txt` and add the line `askpass my-password.txt` to the `my-server-name.conf` file.
2. Put all relevant files to the `/etc/openvpn` folder.
3. Start a particular VPN client (using systemd) by running `sudo systemct start openvpn@my-server-name`.
4. If something is not working, you can monitor openvpn output in the `/var/log/syslog` file.

Alternatively, you could just run `openvpn --config /etc/openvpn/my-server-name.conf`.

**See also:**

- [Is it possible to route only Torrent traffic through VPN?](https://www.reddit.com/r/raspberry_pi/comments/4ahjgq/is_it_possible_to_route_only_torrent_traffic/)
- [Bypass VPN for HTTP/HTTPS traffic on Ubuntu?](https://serverfault.com/questions/726615/bypass-vpn-for-http-https-traffic-on-ubuntu/)

### WireGuard

Supposedly much more efficient.

## Firewall

- iptables
- wireguard

## Glossary

- Network bridge — Connects two networks either using software or hardware methods.
