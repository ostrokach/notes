# MonetDB

## Embedded Python

[IN-DATABASE ANALYTICS WITH PYTHON AND MONETDB](https://ostrokach.gitlab.io/notes-private/files/In-Database_analytics_with_python_and_MonetDB_EN.pdf)

## Cons

- No compression (except for repetitive strings which get converted into a dictionary).
