# Overview

<figure align="center">
<img src="database-island.jpg" />
<figcaption>Source: <a href="https://www.oreilly.com/content/drawing-a-map-of-distributed-data-systems/">https://www.oreilly.com/content/drawing-a-map-of-distributed-data-systems/</a></figcaption>
</figure>

## Textbooks

- [Designing Data Intensive Applications: the Big Ideas Behind Reliable Scalable and Maintainable Systems](https://www.amazon.ca/Designing-Data-Intensive-Applications-Reliable-Maintainable/dp/1449373321)

## File stores

- [Ambry](http://ambry.io/) - Data packaging and management system. Python 2 only. ([Source Code](https://github.com/CivicKnowledge/ambry))
- [ROOT](https://root.cern.ch/) - A modular scientific software framework. It provides all the functionalities needed to deal with big data processing, statistical analysis, visualisation and storage. It is mainly written in C++ but integrated with other languages such as Python and R.

#### Parquet

- [fastparquet](https://fastparquet.readthedocs.io/en/latest/) - ([Source Code](https://github.com/dask/fastparquet))
- [pyarrow.parquet](https://arrow.apache.org/docs/python/parquet.html) - ...

#### SQLite

- SQLite - Best file-based OLTP database.

#### HDF5

- [PyTables](http://www.pytables.org/) - Pretty good option. Has Pandas bindings through the [HDFStore](http://pandas.pydata.org/pandas-docs/stable/io.html#io-hdf5) class.
- [h5py](https://github.com/h5py/h5py) - The h5py package is a Pythonic interface to the HDF5 binary data format. More low-level than **PyTables**.

#### Benchmarks

- [Efficient DataFrame Storage with Apache Parquet](https://tech.blue-yonder.com/efficient-dataframe-storage-with-apache-parquet/)

## Row-oriented

#### [MySQL](https://www.mysql.com/)

- Easiest to set-up and maintain.
- [Move InnoDB tables](http://dev.mysql.com/doc/refman/5.7/en/innodb-transportable-tablespace-examples.html)
- [Move MyISAM tables]()

#### [MariaDB](https://mariadb.org)

- Fork of MySQL by original MySQL developers.
- Can access CSV files directly using the [CONNECT engine](https://mariadb.com/kb/en/mariadb/connect-csv-and-fmt-table-types/).

#### [PostgreSQL](http://www.postgresql.org/)

- [Foreign data wrappers (FDW)](https://wiki.postgresql.org/wiki/Foreign_data_wrappers)
- Use [multicorn](http://multicorn.org/) to write FDW in Python.
- Use [MADlib](https://github.com/apache/incubator-madlib) for scalable in-database analytics.
  - [The MADlib Analytics Library](http://www.eecs.berkeley.edu/Pubs/TechRpts/2012/EECS-2012-38.pdf)

## Column-oriented

#### [MariaDB ColumnStore](https://mariadb.com/kb/en/mariadb/mariadb-columnstore/)

- Probably the best columnstore in the MySQL family.

#### [MonetDB](https://www.monetdb.org/)

- MonetDB is an open source column-oriented database management system.
- [SciQL](http://www.scilens.org/Resources/SciQL.html) provides support for arrays.
- Allows [embedding python code](https://www.monetdb.org/blog/embedded-pythonnumpy-monetdb).
- Supports [SAM/BAM files](https://www.monetdb.org/BAM).
  - Requiers [recompiling](https://www.monetdb.org/Documentation/Extensions/LifeScience/install) samtools with `-fPIC` compiles flags.
- Some work on [Data Valuts](https://www.monetdb.org/Documentation/Extensions/DataVaults)
  - Adds support for _external scientific formats_.
  - Access data _just-in-time_.
  - [Data Vaults: a Database Welcome to Scientific File
    Repositories](http://oai.cwi.nl/oai/asset/21397/21397B.pdf)

#### [Infobright](http://www.infobright.org/)

- Column-oriented fork of **MySQL**.
- Infobright Community Edition 4.0.7 was released on 2012, and is based on MySQL 5.1 (very old).
- [Enterprise edition](https://www.infobright.com) has [many more features](https://www.infobright.org/index.php/Learn-More/ICE_IEE_Comparison/).

#### [InfiniDB](http://infinidb.co/)

- Column-oriented fork of [MySQL](#mysql).
- Went bankrupt and joined [MariaDB](#mariadb).

#### [CitusDB](https://www.citusdata.com/)

- Citus horizontally scales [PostgreSQL](#postgresql) across commodity servers using sharding and replication. Its query engine parallelizes incoming SQL queries across these servers to enable real-time responses on large datasets.
- Citus extends the underlying database rather than forking it, which gives developers and enterprises the power and familiarity of a traditional relational database. As an extension, Citus supports new PostgreSQL releases, allowing users to benefit from new features while maintaining compatibility with existing PostgreSQL tools.
- Citrus [cstore-fdw](https://www.citusdata.com/community/cstore-fdw) allows [creating column-oriented tables](https://www.citusdata.com/blog/76-postgresql-columnar-store-for-analytics) in PostgreSQL ([GitHub](https://github.com/citusdata/cstore_fdw)).
- [GitHub](https://github.com/citusdata/citus), [Docs](https://www.citusdata.com/docs/citus/5.0/reference/citus_sql_reference.html)

#### [GreenPlum](http://pivotal.io/big-data/pivotal-greenplum)

- Pivotal Greenplum is an advanced, fully featured, open source data warehouse. It provides powerful and rapid analytics on petabyte scale data volumes. Uniquely geared toward big data analytics, Greenplum is powered by the world’s most advanced cost-based query optimizer delivering high analytical query performance on large data volumes. Based on **PostgreSQL**.
- Part of the Open Source [Pivotal Big Data Suite](http://pivotal.io/big-data/pivotal-big-data-suite)
- [GitHub](https://github.com/greenplum-db/gpdb)

#### [MemSQL](http://www.memsql.com/)

- "The Fastest In-Memory Database."
- MemSQL is a distributed In-Memory Database that lets you process transactions and run analytics in real-time, using SQL.

#### [LucidDB](http://luciddb.sourceforge.net/)

- LucidDB is the first and only open-source RDBMS purpose-built entirely for data warehousing and business intelligence. It is based on architectural cornerstones such as column-store, bitmap indexing, hash join/aggregation, and page-level multiversioning. Most database systems (both proprietary and open-source) start life with a focus on transaction processing capabilities, then get analytical capabilities bolted on as an afterthought (if at all). By contrast, every component of LucidDB was designed with the requirements of flexible, high-performance data integration and sophisticated query processing in mind. Moreover, comprehensiveness within the focused scope of its architecture means simplicity for the user: no DBA required.
- Superseded by Apache Calcite.

#### [Apache Calcite](http://calcite.apache.org/)

- Apache Calcite is a dynamic data management framework.
- It contains many of the pieces that comprise a typical database management system, but omits some key functions: storage of data, algorithms to process data, and a repository for storing metadata.
- Java.

#### [Druid](http://druid.io)

- Column oriented distributed data store ideal for powering interactive applications
- Java, [GitHub](https://github.com/druid-io/druid/)

## Array

[image:13 align:right][arrays in database systems, the next frontier?](http://www.nesc.ac.uk/talks/1130/SciQLintro_Kersten.pdf)

#### [SciQL](http://www.scilens.org/Resources/SciQL.html)

- An add-on for MonetDB adding support for storing and querying arrays.

#### [SciDB](http://www.paradigm4.com/)

- Difficult to compile / install if not enterprise.
- Data versioning.

#### [RasDaMan](http://www.rasdaman.org/)

- rasdaman ("raster data manager") allows storing and querying massive multi-dimensional ?arrays, such as sensor, image, simulation, and statistics data appearing in domains like earth, space, and life science. This worldwide leading array analytics engine distinguishes itself by its flexibility, performance, and scalability. Rasdaman can process arrays residing in file system directories as well as in databases.

#### [Apache Kylin](http://kylin.apache.org/)

## Hadoop

### Distributions

#### [MapR](https://www.mapr.com/)

#### [Cloudera](http://www.cloudera.com/)

#### [Hortonworks](http://hortonworks.com/)

- By the creators of hadoop.

### Software

- Hive -
- [Impala](https://www.cloudera.com/products/apache-hadoop/impala.html) - Cloudera delivers the modern platform for data management analytics.
- Drill -

## Graph

- [`dgraph`](https://github.com/dgraph-io/dgraph) — Native GraphQL Database with graph backend.

## NoSQL

- [Spark](https://github.com/apache/spark) - Fast and general engine for big data processing, with built-in modules for streaming, SQL, machine learning and graph processing. Create pipelines where you load things into memory only once. [Databricks](https://databricks.com/) has offers hosted solution integrated with AWS.
- [Cassandra](https://github.com/apache/cassandra) - Distributed database good for high write workload. Devloped by [DataStax](http://www.datastax.com/).
- [Scylla](https://github.com/scylladb/scylla) - Cassandra re-written in C++.

## Benchmarks

- [Citus Data cstore_fdw (PostgreSQL Column Store) vs. MonetDB TPC-H Shootout](https://www.monetdb.org/content/citusdb-postgresql-column-store-vs-monetdb-tpc-h-shootout)
  - MonetDB is faster than column-oriented PostgreSQL (using cstore_fdw).
- [Quick comparison of MyISAM, Infobright, and MonetDB](https://www.percona.com/blog/2009/09/29/quick-comparison-of-myisam-infobright-and-monetdb/)
  - MonetDB wins in performance.
  - Infobright wins in table size.
- [Marc Fiume Thesis (page 113)](https://tspace.library.utoronto.ca/handle/1807/69278)
  - Infobright has much smaller tables.
- [Data Management and Data Processing Support on Array-Based Scientific Data](https://etd.ohiolink.edu/!etd.send_file?accession=osu1436157356&disposition=inline)
  - HDF5 vs MonetDB and others.
