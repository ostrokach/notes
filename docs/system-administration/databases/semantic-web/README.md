# Semantic Web

## Differences between triple stores and graph databases

### RDF

W3C standard for data exchange

RDF is a key tech for developing the Semantic Web

- Publish data in well-defined format that agents (software) can consude
- Stored in triplet stores, quad stores, Semantic Graph Databases
- Subject - Predicate - Object
- **Vertices**
  - Resources: URIs
  - Attribute Values: Literal Values
- **Edges**
  - Relationships: URIs
  - Relationships cannot have attributes and are unique (no duplicates)

### LPG (Neo3)

Efficient storage for fast querying and fast traversal

- Labeled property graph
- Became Neo4J
- Nodes have ids and properties
- Edges have ids, type, and properties
- Nodes and Edges have "internal structure"

## Resources

### DBpedia

- [Source code](https://github.com/dbpedia/)
- [Downloads](http://wiki.dbpedia.org/develop/datasets/) - `CC BY 3.0`

### YAGO

YAGO is a large semantic knowledge base, derived from Wikipedia, WordNet, WikiData, GeoNames, and other data sources.

- [Source code](https://github.com/yago-naga/yago3) - `GPLv3`
- [Downloads](https://www.mpi-inf.mpg.de/departments/databases-and-information-systems/research/yago-naga/yago/) - `CC BY 3.0`
