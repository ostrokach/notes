# Home assistant

## Network video recorder (NVR)

- [`frigate`](https://github.com/blakeblackshear/frigate) ★ → NVR with realtime local object detection for IP cameras. Works great with Home Assistant.
