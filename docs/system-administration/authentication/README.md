# Authentication

- [`FreeIPA`](https://www.freeipa.org/page/Main_Page) — Provides LDAP and Kerberos setup for Linux. Note that Ubuntu 20.10 comes with native support for LDAP for authentication.

## LDAP

- [RedHat authconfig-ldap](https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/7/html/System-Level_Authentication_Guide/authconfig-ldap.html)

### Kerberos

- Can be used with either NIS or LDAP for single sign-on.

### Additional resources

- [StackOverflow: Simple, centralized user management on a small LAN — NIS or LDAP?](https://serverfault.com/a/550064/241418) — _"I don't think anybody uses NIS anymore — or at least, wants to"_. Use LDAP. Red Hat has a good installation guide. _Volt_ has built-in support for LDAP.
- LDAP is more secure than NIS.

## NIS

<!-- prettier-ignore -->
!!! tip
    NIS is generally considered to be outdated and obsolete. It is not recommended for production deployments. Use something like LDAP instead.

### Files that need to be updated

- `/etc/yp.conf` — This is where `ypserver` is set.
- `/etc/hosts.conf` — Had an obsolete `order` keyword which allows you to select in which order you process hostnames.
- `/etc/nsswitch.conf` — Default file for domain name resolution.
- `/etc/network/interfaces` — Static IP address and DNS nameserver.
- `/etc/passwd` — Add `+::::::` at the bottom to sync all users. Individual users can be whitelisted or blacklisted using `+username::::::` or `-username::::::`, respectively.
- `/etc/group` — Add `+:::` at the bottom to sync all groups.
- `/etc/shadow` — Add `+::::::` at the bottom to sync user passwords.

### System daemons

- `dnsmasq` — Listens on port 53 for requests.

### System commands

```bash
# Start portmap
sudo service portmap start

# Start NIS client
sudo service ypbind Start
```

### Additional resources

- [nis.debian.howto](http://www.linux-nis.org/doc/nis.debian.howto)
- [ArchLinux NIS](https://wiki.archlinux.org/index.php/NIS)
- [7. Setting Up the NIS Client](http://www.tldp.org/HOWTO/NIS-HOWTO/settingup_client.html)
