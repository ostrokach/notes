# Chrome

## Awesome add-ons

- [`The Great Suspender`](https://github.com/greatsuspender/thegreatsuspender) — Suspend idle Chrome tabs.
