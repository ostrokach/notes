# Filesystems

## Cache

#### Cache-FS

- Mainly for caching NFS mounts.
- RedHat [documentation](https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/7/html/Storage_Administration_Guide/ch-fscache.html).

## Centralized

### NFS

Use `nfsnobody` user and group for folders that should be accessed by root users on client systems (including docker).

- [NFS Volume Security](https://docs.openshift.com/enterprise/3.2/install_config/persistent_storage/persistent_storage_nfs.html#nfs-volume-security)
- [Do Not Use the `no_root_squash` Option](https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/6/html/Security_Guide/sect-Security_Guide-Securing_NFS-Do_Not_Use_the_no_root_squash_Option.html)

### ZFS

**Advantages of ZFS:**

- Transactional file system: always consistent on disk
- Provable data integrity
- Snapshots
- Mirroring

**See also:**

- [ZFS: The Last Word In File Systems](http://wiki.illumos.org/download/attachments/1146951/zfs_last.pdf)
- [ZFS send to external backup drive](https://forums.freenas.org/index.php?threads/zfs-send-to-external-backup-drive.17850/)
- [Shrinking a ZFS root pool](https://blog.grem.de/sysadmin/Shrinking-ZFS-Pool-2014-05-29-21-00.html)
- [HowTo Create Striped Mirror Vdev ZPool](http://www.zfsbuild.com/2010/06/03/howto-create-striped-mirror-vdev-pool)
- [Create a ZFS volume on Ubuntu](https://www.jamescoyle.net/how-to/478-create-a-zfs-volume-on-ubuntu)
- [ZFS and Ubuntu Home Server howto](https://www.latentexistence.me.uk/zfs-and-ubuntu-home-server-howto/)
- [Why RAID 5 stops working in 2009](http://www.zdnet.com/article/why-raid-5-stops-working-in-2009/)
- [ZFS: You should use mirror vdevs, not RAIDZ](http://jrs-s.net/2015/02/06/zfs-you-should-use-mirror-vdevs-not-raidz/)
- [ZFS Datasets dissappear on reboot](https://serverfault.com/questions/732184/zfs-datasets-dissappear-on-reboot)

### Other

- [`zram`](https://en.wikipedia.org/wiki/Zram) — Create compressed block device in RAM.

## Distributed

### Ceph

- Started off as an "object store".
- Common in OpenStack deployments.

### Gluster

https://www.gluster.org/

- Share-nothing architecture.
- Under active development (was bought by Red Hat).
- Uses [nfs-ganesha](https://github.com/nfs-ganesha/nfs-ganesha/wiki) to create (or proxy) an NFS server in user space.

### Magic Pocket

Proprietary system used at Drop Box.

### MapR-FS

- Part of the [Converged Data Platform](https://www.mapr.com/products/mapr-distribution-editions)

### SeaweedFS

https://github.com/chrislusf/seaweedfs

A simple and highly scalable distributed file system. There are two objectives: to store billions of files! to serve the files fast! Instead of supporting full POSIX file system semantics, SeaweedFS choose to implement only a key~file mapping. Similar to the word "NoSQL", you can call it as "NoFS".
