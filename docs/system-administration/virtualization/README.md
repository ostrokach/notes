# Virtualization

## VM creation and management

- [`QuickEMU`](https://github.com/quickemu-project/quickemu) → Quickly create and run optimised Windows, macOS and Linux desktop virtual machines. Supports Windows and MacOS.
- [`Vagrant`](https://github.com/hashicorp/vagrant) → Vagrant is a tool for building and distributing development environments.

## Hypervisors

- Emulate virtual hardware.
- Heavy system requirements.

### Xen

### KVM

## Containers

https://linuxcontainers.org/

- Sit on top of a single kernel.
- Use LXC / `cgroups` to "boot up" from a different place on the hard drive--the container.

### LXD

- Quickly spin up linux-based OS in kernel space.
- Integrated with ZFS for live backups.

### Docker

- Designed to host individual _apps_, rather than the entire stack.
- Uses ephemeral disk storage unless you use volumes.
