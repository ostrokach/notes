# Compression

## Compression algorithms

- [`brotli`] ★ — Generally better compression than `gzip` with comparable runtime.
- [`bzip2`](https://www.sourceware.org/bzip2/) — Freely available, patent free, high-quality data compressor.
- [`FastLZ`](https://github.com/ariya/FastLZ) — Small & portable byte-aligned LZ77 compression.
- [`gzip`](https://www.gzip.org/) — Single-file/stream lossless data compression utility.
- [`libarchive`](https://github.com/libarchive/libarchive) ★ — Provides a common interface to different compression algorithms. Includes the `bsdcat`, `bsdcpio`, and `bsdtar` binaries.
- [`lrzip`](https://github.com/ckolivas/lrzip) — Long-range zip. Your choice of compression (`lzma`/`bzip2`/`gzip`). Best if you want to compress very large text files. Not that much development and lingering bugs.
- [`lz4`](https://github.com/lz4/lz4) ★ — Probably the best algorithm for _fast_ compression. Faster than `gzip`. Default for ZFS.
- [`lzo`](http://www.oberhumer.com/opensource/lzo/) — "Offers pretty fast compression and _extremely_ fast decompression."
- [`lzturbo`](https://sites.google.com/site/powturbo/home)
- [`p7zip`](https://sourceforge.net/projects/p7zip/) — Best for indexable files. Indexed: can extract single files. CLI is cumbersome on Linux. Use `-ms=off` to disable solid compression (compressing multiple files together). This allows files to be extracted on at a time. [`LZMA2`, `Indexed`]
- [`pixz`](https://github.com/vasi/pixz) — Pixz (pronounced pixie) is a parallel, indexing version of `xz`. Indexed: can extract single files. Better CLI than 7z. Got a fatal error last time I tried. [`LZMA2`, `Indexed`]
- [`QuickLZ`](http://www.quicklz.com/) — QuickLZ is the world's fastest compression library, reaching 308 Mbyte/s per core.
- [`snappy`](http://google.github.io/snappy/) — Default in Apache Parquet files. Supposed to be faster than `gzip` at the expense of lower compression ratios.
- [`squashfs`](https://www.kernel.org/doc/html/latest/filesystems/squashfs.html) — Compressed file system. Can be accessed in userspace using [`squashfuse`](https://github.com/vasi/squashfuse). [`gzip`, `lzma`, `lzo`, `xz`, `Indexed`]
- [`xz`] / [`lzma`] — Good compression, but slow. Slowest / best compression ratio. Faster decompression than `bzip2`.
- [`zstd`](https://facebook.github.io/zstd/) ★ — Probably the best algorithm for _good_ compression. Compression can be a bit or a lot slower than `gzip`, depending on compression level. Decompression always reasonably fast. Developed at Facebook.

**See also:**

- http://askubuntu.com/a/258228/229420
- https://www.digitalocean.com/community/tutorials/an-introduction-to-file-compression-tools-on-linux-servers
- http://unix.stackexchange.com/a/78273/78327
- http://stackoverflow.com/a/8060136/2063031
