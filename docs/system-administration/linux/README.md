# Linux

## Software

- https://github.com/ibraheemdev/modern-unix

### CLI

- [`iftop`](https://linux.die.net/man/8/iftop)
- [`nethogs`](https://github.com/raboof/nethogs) — [`📖`](https://linux.die.net/man/8/nethogs)
- `less` — View a file. `F`: Track the end of a file (less efficient then `tail -f` because it uses polling instead of inotify). `Shift-S`: No line wrapping. `-#`, `--shift`: Control number of lines by which to move left or right.

### Desktop

- [`etcher`](https://github.com/balena-io/etcher) — Flash OS images to SD cards & USB drives, safely and easily. <br>![](https://img.shields.io/github/stars/balena-io/etcher.svg?style=social)
- [`gPodder`](https://github.com/gpodder/gpodder) — Best podcast manager for Linux. Can rename files after downloading using an [extension](http://wiki.gpodder.org/wiki/Extensions/RenameAfterDownload). `Python` <br>![](https://img.shields.io/github/stars/gpodder/gpodder.svg?style=social)
- [`redshift`](https://github.com/jonls/redshift) — Change color of screen at night. For example, for Toronto, run: `redshift -l 43.6532:-79.3832`. `C` <br>![](https://img.shields.io/github/stars/jonls/redshift.svg?style=social)
- [`TunesViewer`](https://github.com/rbrito/tunesviewer) — Find and download podcasts from iTunes. `Python` <br>![](https://img.shields.io/github/stars/rbrito/tunesviewer.svg?style=social)

### Daemons

- [`earlyoom`](https://github.com/rfjakob/earlyoom) — Terminate memory hogs once you start running out of memory. For alternatives, see: [Linux Unplugged E348 (OK OOMer)](https://linuxunplugged.com/348). [`📖`](http://manpages.ubuntu.com/manpages/focal/man1/earlyoom.1.html)
- [`tlp`](https://github.com/linrunner/TLP) — Optimize Linux laptop battery life. [`📖`](https://linrunner.de/tlp/)

### Media

- [`pipewire`](https://gitlab.freedesktop.org/pipewire/pipewire) → Multimedia processing graphs.
- [`pulseaudio`](https://gitlab.freedesktop.org/pulseaudio/pulseaudio)
