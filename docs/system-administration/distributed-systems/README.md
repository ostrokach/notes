# Distributed Systems

## Algorithms

### Consensus

- `Paxos`
- `Raft` - Much easier to implement correctly than `Paxos`.

### Information spreading

- `Gossip` - Each node passes incoming information to two or more other nodes.

## Data structures

- [Conflict-free replicated data type](https://en.wikipedia.org/wiki/Conflict-free_replicated_data_type).

## Software

### RPC

- [`Finagle`]() - Developed at Twitter. [`Scala`]
- [`gRPC`]() - Bindings in many different languages.

### Service meshes

- [`Istio`](https://istio.io/) - Can use `linkerd` or `envoy` as a service proxy (data plane).
- [`Consul`](https://www.consul.io/) -

### Data plane

- [`Envoy`](https://www.envoyproxy.io/) - Developed at Lyft.
- [`LinkerD`](https://linkerd.io/) - Version 2 (formerly known as Conduit) is a complete rewrite using Rust and Go. Part of the CNCF. [`GO`, `Rust`]
- [`Nginx`]() - Not used as much anymore.
