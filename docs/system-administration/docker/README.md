# Docker

## Docker commands

- [`docker system prune`](https://docs.docker.com/engine/reference/commandline/system_prune/) - Remove unused data. Useful when docker eats all available disk space on our computer. Pass the `--all` argument to delete _all unused images_, not just dangling images.

  ```bash
  docker system prune --all
  ```

## Container init programs

- [`runit`](http://smarden.org/runit/) - A UNIX init scheme with service supervision. ([Source Code](https://github.com/vulk/runit)) `C`
- [`tini`](https://github.com/krallin/tini) - A tiny but valid init for containers. Used by conda-forge. Probably the best choice. `C`
- [`s6`](http://skarnet.org/software/s6/) - The s6 supervision suite. ([Source Code](https://github.com/skarnet/s6)) `C`
- [`supervisor`](http://supervisord.org/) - Does not pick up zombie processes. `Python`

### Docker machine

- In order to log in to one of the worker machines, use ssh certificates from the `/root/.docker/machine/machines` folder.

## Troubleshooting

Docker does not correctly detect the MTU (maximum transfer unit) of the container. It must be specified manually in the `/lib/systemd/system/docker.service` file:

```ini
ExecStart=/usr/bin/docker daemon -H fd:// –mtu=1400
```

## Additional resources

- [Automated Nginx Reverse Proxy for Docker](http://jasonwilder.com/blog/2014/03/25/automated-nginx-reverse-proxy-for-docker/)
- [nginx-proxy](https://github.com/jwilder/nginx-proxy)
  - Automated nginx proxy for Docker containers using docker-gen
  - Docker image: <https://hub.docker.com/r/jwilder/nginx-proxy/>
- [docker-gen](https://github.com/jwilder/docker-gen)
  - Generate files from docker container meta-data
