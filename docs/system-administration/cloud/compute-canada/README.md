# Compute Canada

## Info

- [Login](https://ccdb.computecanada.ca/)
- [Usage](https://ccdb.computecanada.ca/me/group_usage)
- [Available Resources](https://www.computecanada.ca/research-portal/accessing-resources/available-resources/)
- [List of Compute Servers](https://www.computecanada.ca/research-portal/national-services/compute/)

## Competitions

- https://www.computecanada.ca/
- [Research Platforms and Portals (RPP)](https://www.computecanada.ca/research-portal/accessing-resources/resource-allocation-competitions/rpp/)
- [Resources for Research Groups (RRG)](https://www.computecanada.ca/research-portal/accessing-resources/resource-allocation-competitions/rrg/)
  - [RRG streams](https://www.computecanada.ca/research-portal/accessing-resources/resource-allocation-competitions/rrg/#streams)
- [Rapid access service](https://www.computecanada.ca/research-portal/accessing-resources/rapid-access-service/)
- [Frequently Asked Questions](https://www.computecanada.ca/research-portal/accessing-resources/resource-allocation-competitions/faq2017/)

## Cloud

- https://east.cloud.computecanada.ca/
- [Cloud Powering Research presentation](https://www.computecanada.ca/wp-content/uploads/2016/03/CC-CloudPoweringResearch-Mar17.2016.pdf)

## Compute

- [ACENET](http://www.ace-net.ca/)
- [CAC](http://www.hpcvl.org/) — Centre for Advanced Computing (formerly HPCVL). Led by Queen's University, and includes Carleton University, University of Ottawa, and the Royal Military College of Canada.
- [Calcul Quebec](http://www.calculquebec.ca) — ([Wiki](https://wiki.calculquebec.ca))
- [SHARCNET](https://www.sharcnet.ca)
- [SciNet](http://www.scinethpc.ca/) — ([Wiki](https://wiki.scinet.utoronto.ca))
- [WestGrid](https://www.westgrid.ca/)

## See also

### University of Toronto

CSLab has compute servers that can be reserved for intensive work (they include GPUs!)

http://support.cs.toronto.edu/ApplicationServices/LinuxComputeServers.shtml
