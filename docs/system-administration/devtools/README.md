# Development tools

## IDEs

### Visual Studio Code

- User settings are stored in `~/.config/Code/`.
- Installed packages are stored in `~/.vscode/extensions<!-- -->`.
- (On Linux) Visual Studio Code sources your `~/.bashrc` file to populate its working environment. If you wish VSCode to execute some code when starting, you can add that code inside `if [[ ! -z "${VSCODE_PID}" ]] ; then ... ; fi` guards. For more info, see: [Feature Request: Ability to explicitly set PATH](https://github.com/Microsoft/vscode/issues/24989#issuecomment-295146588).

### Alternatives for C / C++

- `Anjuta` — `autotools` support. [`GTK`]
- `CLion` — cMake only.
- `Code::blocks`
- `CodeLite` — cMake support.
- `Dev-C++`
- `Eclipse CDT`

## GitLab

- To change port, edit `gitlab.rt` and add `external_url "domain:8888"` ([source](https://github.com/gitlabhq/gitlabhq/issues/6581))

We can add multiline scripts in `.gitlab-ci.yml` files as follows:

```yaml
.foo:
  script: &foo
    - echo "Do"
    - echo "Something"
    - echo "Multi-line"

.bar:
  script: &bar
    - echo "Some"
    - echo "More"
    - echo "Actions"

test:
  script:
    - *foo
    - *bar
```

For more info, see <https://gitlab.com/gitlab-org/gitlab-ce/issues/24235#note_140649852>.

### Using tags

Tags are now available in GitLab (e.g. <https://gitlab.com/explore/projects?tag=clojure>). See [`gitlab-org/gitlab#14220](https://gitlab.com/gitlab-org/gitlab/-/issues/14220) for more info.

### Gitlab runner

#### Docker machine executor

Use [the following tutorial](https://fuga.cloud/academy/tutorials/autoscaling-your-gitlab-runners-on-fuga/) for setting up gitlab-runner + docker-machine workers on openstack.

For `docker+machine`, configurations should look something like the following:

```toml
concurrent = 12
check_interval = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = "docker-machine-1"
  url = "https://gitlab.com/"
  token = "XXXXX"
  executor = "docker+machine"
  limit = 7
  [runners.docker]
    tls_verify = false
    image = "ubuntu:latest"
    privileged = false
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/cache"]
    pull_policy = ["if-not-present"]
    shm_size = 0
  [runners.cache]
  [runners.machine]
    IdleCount = 0
    IdleTime = 3600
    MaxBuilds = 60
    MachineDriver = "openstack"
    MachineName = "auto-scale-%s"
    MachineOptions = [
        "openstack-auth-url=https://arbutus.cloud.computecanada.ca:5000/v3",
        "openstack-domain-name=XXXXX",
        "openstack-flavor-name=c8-60gb-186",
        "openstack-image-name=gitlab-ci-base",
        "openstack-insecure=true",
        "openstack-keypair-name=gitlab",
        "openstack-net-name=rrg-pmkim-network",
        "openstack-password=XXXXX",
        "openstack-private-key-file=/home/ubuntu/.ssh/gitlab.pem",
        "openstack-region=XXXXX",
        "openstack-sec-groups=default,gitlabrunner",
        "openstack-ssh-user=ubuntu",
        "openstack-tenant-id=XXXXX",
        "openstack-tenant-name=XXXXX",
        "openstack-user-data-file=/home/gitlab-runner/docker_on_mnt.sh"
        "openstack-username=strokach",
    ]
    OffPeakTimezone = ""
    OffPeakIdleCount = 0
    OffPeakIdleTime = 0
```

The following should be executed after launching a new VM and before installing docker:

```bash
#!/bin/bash
# /home/gitlab-runner/docker_on_mnt.sh
sudo mkdir /mnt/docker
sudo ln -s /mnt/docker /var/lib/docker
```

Alternatively, modify the base image to include a custom Docker `daemon.json` file:

```bash
#!/bin/bash
# /home/gitlab-runner/docker_on_mnt.sh
sudo mkdir /etc/docker/
echo '{ "data-root": "/mnt" }' | sudo tee /etc/docker/daemon.json
```

Test out your settings manually before starting `gitlab-runner`:

```bash
docker-machine create deleteme --driver openstack {all-other-options}
```
