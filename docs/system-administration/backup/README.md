# Backup and recovery

## Backup tools

- `rsync` — Source and destination can be mirrored using something like the following: <br />`rsync -aAHX ./sourc ./destination --info=progress2`
- [`restic`](https://github.com/restic/restic) — Native support for backing up to cloud storage providers. `GO` <br />![](https://img.shields.io/github/stars/restic/restic.svg?style=social&label=star)
- [`borg`](https://github.com/borgbackup/borg) — Deduplication, compression, incremental backups. `Python` <br />![](https://img.shields.io/github/stars/borgbackup/borg.svg?style=social&label=Star)
- [`rsnapshot`](https://github.com/rsnapshot/rsnapshot) — Incremental backups. `Perl` <br />![](https://img.shields.io/github/stars/rsnapshot/rsnapshot.svg?style=social&label=Star)
- [`backintime`](https://github.com/bit-team/backintime) — ... `Python` <br />![](https://img.shields.io/github/stars/bit-team/backintime.svg?style=social&label=Star)
- [`hat-backup`](https://github.com/google/hat-backup) — Deduplication, compression, incremental backups. `Rust` <br />![](https://img.shields.io/github/stars/google/hat-backup.svg?style=social&label=Star)

**See also:**

- [AskUbuntu: Comparison of backup tools](http://askubuntu.com/questions/2596/comparison-of-backup-tools).

## Hard drive cloning

- [rsync](https://rsync.samba.org/) — Recursive sync. `C`

  ```bash
  rsync -aAXv --filter=':- .stignore' /home/username /backup/username
  ```

- [`dd`](https://www.gnu.org/software/coreutils/coreutils.html) — Copy data block by block. `C`
- [`clonezilla`](http://clonezilla.org/) — Uses `partclone`, `partimage`, `ntfsclone`, `dd`, etc. to copy files from old disk / partition to new disk / partition. Can clone entire disks / partitions to images. `C`

## Hard drive recovery

- [`ddrescue`](http://www.gnu.org/software/ddrescue/ddrescue.html) — Install on Ubuntu using: `apt-get install gddrescue`. `C`

## File recovery

### Free

Programs:

- [TestDisk](http://www.cgsecurity.org/wiki/TestDisk)
- [PhotoRec](http://www.cgsecurity.org/wiki/PhotoRec)
- [PCInspector](http://www.pcinspector.de/)
- [Recuva](http://www.piriform.com/recuva)
- [EaseUS Deleted File Recovery](http://www.easeus-deletedrecovery.com/)

Sources:

- [What data recovery tools do the pros use?](http://ask.slashdot.org/story/09/06/02/1332222/what-data-recovery-tools-do-the-pros-use)

Blogs:

- [Ubuntu Data Recovery](https://help.ubuntu.com/community/DataRecovery)

### Commercial

- [Stellar Phoenix Windows Data Recovery — Professional](http://www.stellarinfo.com/partition-recovery-software.php)
- [Free Any Data Recovery](http://www.any-data-recovery.com/product/datarecoverystandard.htm)
- [EasUS Data Recovery Wizard Free 9.8](http://www.easeus.com/datarecoverywizard/free-data-recovery-software.htm)
- Tuxera Recovery
- Ontrack Easy Recovery
- R-Studio (Linux option)
