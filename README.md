# Notes

## Create a conda environment with required packages

```bash
conda create -n mkdocs 'python=3.8'
conda activate mkdocs
pip install mkdocs mkdocs-material mkdocs-awesome-pages-plugin \
    python-markdown-math mdx_truly_sane_lists
```
